# README #

Things I would change to improve this code base.
 switch to injection instead of singletons
 start using smart pointers instead of raw ones
 switch from a single window to allow multiple windows, this could be great while developing, example, create a window for seeing the hierarchy

This is a small 2D Games Engine created by Alice Thorntonsmith, for a bit of fun.
It uses SDL to create and render the application to a Window.

To Run, open the project in Visual Studio and build.
(for Release you'll need to add the Assets folder to the Release folder
so the .exe can access the files from there)

Currently the Project Contains:

Scenegraph Hierarchy with GameObjects,
Components which can be attached to GameObjects,
Simple Particles that can be loaded from file,
Json Loading,
Very simple physics detection.

Still to do:

Physics:
 Have the engine take control of the movement of objects
 Have the engine react to collision rather than just sending a signal
 
Audio:
 All of it
 
Json:
 Saving
 
Tools:
 Look at creating debug tools, which show the current scenegraph hierarcy
 A particle creator which allows a user to edit and see the particle effect instantly, and then save the settings to file.