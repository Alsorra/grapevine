/*
*
* Grapevine Games
*
* Written and Owned by Alice Thorntonsmith
*
*/

#include "Breakout/GameApplication.h"

#include <vld.h>

int main(int argc, char* args[])
{
	Breakout::GameApplication application;

	application.Run();

	return 0;
}