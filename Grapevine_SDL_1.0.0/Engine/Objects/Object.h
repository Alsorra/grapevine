/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_OBJECT
#define GRAPEVINE_SDL_OBJECT

#include <string>

namespace Grapevine
{
	class Object
	{
	public:
		Object(std::string _name);

		virtual ~Object();

		bool operator==(const Object& _other);

		bool operator!=(const Object& _other);

	public:
		std::string m_name;

		bool m_enabled;
	};
}

#endif