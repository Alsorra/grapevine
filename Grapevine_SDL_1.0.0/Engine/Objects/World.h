/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include "GameObject.h"

namespace Grapevine
{
	class PhysicsWorldComponent;

	class World : public GameObject
	{
	public:
		World();

		PhysicsWorldComponent* GetPhysicsWorldComponent() const;

	private:
		PhysicsWorldComponent* m_worldPhysics;
	};
}