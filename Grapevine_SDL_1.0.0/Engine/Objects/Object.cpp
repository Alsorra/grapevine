/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Object.h"

namespace Grapevine
{
	Object::Object(std::string _name)
		: m_name(_name)
		, m_enabled(true)
	{
	}
	Object::~Object()
	{
	}

	bool Object::operator==(const Object& _other)
	{
		return this == &_other;
	}
	bool Object::operator!=(const Object& _other)
	{
		return this != &_other;
	}
}