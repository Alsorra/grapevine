/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "ParticleObject.h"

#include "../Components/Behaviours/Particles/ParticleBehaviour.h"
#include "../Components/Renderers/ParticleRenderer.h"

namespace Grapevine
{
	ParticleObject::ParticleObject(const std::string& _name)
		: GameObject(_name)
		, m_behaviour(new ParticleBehaviour("particle_test"))
	{
		this->AddComponent(m_behaviour);
		this->AddComponent(new ParticleRenderer(m_behaviour));

		m_behaviour->StartEmitting(false, 0);
	}

	ParticleObject::ParticleObject(const std::string& _name, const std::string& _settingsFile)
		: GameObject(_name)
		, m_behaviour(new ParticleBehaviour(_settingsFile))
	{
		this->AddComponent(m_behaviour);
		this->AddComponent(new ParticleRenderer(m_behaviour));
	}

	ParticleObject::~ParticleObject()
	{
	}

	void ParticleObject::SetParticleTexture(Texture* _texture)
	{
		ParticleRenderer* renderer = GetComponent<ParticleRenderer>();

		renderer->SetTexture(_texture);
	}

	void ParticleObject::SetParticleColour(const Colour& _colour)
	{
		ParticleRenderer* renderer = GetComponent<ParticleRenderer>();

		renderer->SetColour(_colour);
	}

	void ParticleObject::StartEmitting(bool _useCounter, int _counter)
	{
		m_behaviour->StartEmitting(_useCounter, _counter);
	}

	void ParticleObject::StopEmitting()
	{
		m_behaviour->StopEmitting();
	}
}