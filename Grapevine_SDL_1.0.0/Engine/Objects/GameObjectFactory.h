/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include <string>

namespace Grapevine
{
	class GameObject;
	class Texture;
	class Transform;
	class TrueTypeFont;

	class GameObjectFactory
	{
	public:
		static GameObject* GenerateEmptyObject();
		static GameObject* GenerateEmptyObject(const Transform& transform);
		static GameObject* GenerateEmptyObject(const std::string& name);
		static GameObject* GenerateEmptyObject(const std::string& name, const Transform& transform);

		static GameObject* GenerateTextObject(TrueTypeFont* font, const std::string& text);
		static GameObject* GenerateTextObject(TrueTypeFont* font, const std::string& text, const Transform& transform);

		static GameObject* GenerateTextureObject(Texture* texture);
		static GameObject* GenerateTextureObject(Texture* texture, const Transform& transform);
	};
}