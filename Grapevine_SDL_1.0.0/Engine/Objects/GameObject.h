/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include "../../Engine/Maths/Transform.h"
#include "../../Engine/Objects/Object.h"

#include <vector>

namespace Grapevine
{
	class Component;
	class HierarchyComponent;
	class RendererComponent;

	class GameObject : public Object
	{
	public:
		GameObject(const std::string& name);

		virtual ~GameObject();

		virtual void Update();

		virtual void Render();

		void AddComponent(Component* component);

		void RemoveComponent(Component* component);

		template <class T>
		T* GetComponent();

		template <class T>
		T* RemoveComponent();

		void SetParent(GameObject* parent);

		Transform& GetTransform() { return m_mathsTransform; }
		const Transform& GetTransform() const { return m_mathsTransform; }

	private:
		Transform m_mathsTransform;

		HierarchyComponent* m_hierarchy;

		RendererComponent* m_renderer;

		std::vector<Component*> m_components;
	};

	template <class T>
	T* GameObject::GetComponent()
	{		
		for (auto& component : m_components)
		{
			T* comp = dynamic_cast<T*>(component);
			if (comp != nullptr)
			{
				return comp;
			}
		}

		return nullptr;
	}

	template <class T>
	T* GameObject::RemoveComponent()
	{
		if (T == HierarchyComponent)
		{
			return nullptr;
		}

		std::vector<Component*>::iterator iter = m_components.begin();
		for(auto& iter = m_components.begin(); iter != m_components.end(); ++iter)
		{
			T* comp = dynamic_cast<T*>(*iter);

			if (comp != nullptr)
			{
				if ((*iter) == m_transform)
				{
					return nullptr;
				}

				m_components.erase(iter);
				(*iter)->m_gameObject = nullptr;
				
				return (*iter);
			}
		}

		return nullptr;
	}
}