/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "GameObjectFactory.h"

#include "../Application/Application.h"
#include "../Application/Window.h"
#include "../Components/Renderers/TextRenderer.h"
#include "../Components/Renderers/TextureRenderer.h"
#include "../Maths/Transform.h"
#include "../Objects/GameObject.h"

namespace Grapevine
{
	GameObject* GameObjectFactory::GenerateEmptyObject()
	{
		return GenerateEmptyObject(Transform());
	}

	GameObject* GameObjectFactory::GenerateEmptyObject(const Transform& transform)
	{
		return GenerateEmptyObject("new object", transform);
	}

	GameObject* GameObjectFactory::GenerateEmptyObject(const std::string& name)
	{
		return GenerateEmptyObject(name, Transform());
	}

	GameObject* GameObjectFactory::GenerateEmptyObject(const std::string& name, const Transform& transform)
	{
		GameObject* object = new GameObject(name);
		object->GetTransform().SetTranslation(transform.GetTranslation());
		object->GetTransform().SetScale(transform.GetScale());
		object->GetTransform().SetRotation(transform.GetRotation());

		object->SetParent(&Application::GetInstance()->GetMainWindow()->GetMainObject());

		return object;
	}

	GameObject* GameObjectFactory::GenerateTextObject(TrueTypeFont* font, const std::string& text)
	{
		return GenerateTextObject(font, text, Transform());
	}

	GameObject* GameObjectFactory::GenerateTextObject(TrueTypeFont* font, const std::string& text, const Transform& transform)
	{
		GameObject* object = new GameObject("new object");
		object->AddComponent(new TextRenderer(font, text));
		object->GetTransform().SetTranslation(transform.GetTranslation());
		object->GetTransform().SetScale(transform.GetScale());
		object->GetTransform().SetRotation(transform.GetRotation());

		object->SetParent(&Application::GetInstance()->GetMainWindow()->GetMainObject());

		return object;
	}

	GameObject* GameObjectFactory::GenerateTextureObject(Texture* texture)
	{
		return GenerateTextureObject(texture, Transform());
	}

	GameObject* GameObjectFactory::GenerateTextureObject(Texture* texture, const Transform& transform)
	{
		GameObject* object = new GameObject("new object");
		object->AddComponent(new TextureRenderer(texture));
		object->GetTransform().SetTranslation(transform.GetTranslation());
		object->GetTransform().SetScale(transform.GetScale());
		object->GetTransform().SetRotation(transform.GetRotation());

		object->SetParent(&Application::GetInstance()->GetMainWindow()->GetMainObject());

		return object;
	}
}