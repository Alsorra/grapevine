/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "World.h"

#include "../Components/Physics/PhysicsWorldComponent.h"

namespace Grapevine
{
	World::World()
		: GameObject("World")
		, m_worldPhysics(new PhysicsWorldComponent())
	{
		AddComponent(m_worldPhysics);
	}

	PhysicsWorldComponent* World::GetPhysicsWorldComponent() const
	{
		return m_worldPhysics;
	}
}