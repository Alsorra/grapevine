/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_PARTICLEOBJECT
#define GRAPEVINE_SDL_PARTICLEOBJECT

#include "../Objects/GameObject.h"

namespace Grapevine
{
	struct Colour;

	class ParticleBehaviour;
	class Texture;

	class ParticleObject : public GameObject
	{
	public:
		ParticleObject(const std::string& _name);

		ParticleObject(const std::string& _name, const std::string& _settingsFile);

		virtual ~ParticleObject();

		void SetParticleTexture(Texture* _texture);

		void SetParticleColour(const Colour& _colour);

		void StartEmitting(bool _useCounter = false, int _counter = 0);

		void StopEmitting();

	private:
		ParticleBehaviour* m_behaviour;
	};
}

#endif