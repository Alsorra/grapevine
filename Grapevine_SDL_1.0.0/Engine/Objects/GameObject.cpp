/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

// this class
#include "GameObject.h"

// includes
#include "../Application/Application.h"
#include "../Application/Renderer.h"
#include "../Application/Window.h"
#include "../Components/HierarchyComponent.h"
#include "../Components/Behaviours/Behaviour.h"
#include "../Components/Renderers/RendererComponent.h"
#include "../Components/Renderers/TextRenderer.h"
#include "../Components/Renderers/TextureRenderer.h"
#include "../Objects/World.h"

namespace Grapevine
{
	// -- GameObject Methods -- //

	GameObject::GameObject(const std::string& name)
		: Object(name)
		, m_hierarchy(new HierarchyComponent())
		, m_renderer(nullptr)
	{
		AddComponent(m_hierarchy);
	}

	GameObject::~GameObject()
	{
		// iterate through all children and delete them
		while (!m_hierarchy->m_children.empty())
		{
			HierarchyComponent* child = m_hierarchy->m_children.back();
			m_hierarchy->m_children.pop_back();

			delete child->GetGameObject();
		}

		// iterate through all components attached and delete them
		while (!m_components.empty())
		{
			delete m_components.back();
			m_components.pop_back();
		}
	}

	void GameObject::Update()
	{
		if (!m_enabled)
		{
			return;
		}

		for (auto& component : m_components)
		{
			Behaviour* behaviour = dynamic_cast<Behaviour*>(component);
			if (behaviour != nullptr && behaviour->m_enabled)
			{
				behaviour->Update();
			}
		}

		for (auto& child : m_hierarchy->m_children)
		{
			child->GetGameObject()->Update();
		}
	}

	void GameObject::Render()
	{
		if (!m_enabled)
		{
			return;
		}

		Renderer::PushTransform(m_mathsTransform);

		if (m_renderer != nullptr && m_renderer->m_enabled)
		{
			m_renderer->Render();
		}

		for (auto& child : m_hierarchy->m_children)
		{
			child->GetGameObject()->Render();
		}

		Renderer::PopTransform();
	}

	void GameObject::AddComponent(Component* component)
	{
		if (component->m_gameObject != nullptr)
		{
			component->m_gameObject->RemoveComponent(component);
		}

		m_components.push_back(component);
		component->m_gameObject = this;

		if (this->m_renderer == nullptr)
		{
			RendererComponent* renderer = dynamic_cast<RendererComponent*>(component);

			if (renderer != nullptr)
			{
				m_renderer = renderer;
			}
		}

		Behaviour* behaviour = dynamic_cast<Behaviour*>(component);
		if (behaviour != nullptr)
		{
			behaviour->Start();
		}
	}

	void GameObject::RemoveComponent(Component* component)
	{
		if (component == m_hierarchy)
		{
			return;
		}

		auto& iter = std::find(m_components.begin(), m_components.end(), component);
		if (iter != m_components.end())
		{
			m_components.erase(iter);

			component->m_gameObject = nullptr;
			if (component == m_renderer)
			{
				m_renderer = nullptr;
			}
		}
	}

	void GameObject::SetParent(GameObject* parent)
	{
		if (parent != nullptr)
		{
			m_hierarchy->SetParent(parent->m_hierarchy);
		}
		else
		{
			m_hierarchy->SetParent(nullptr);
		}
	}
}