/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_INPUTKEY
#define GRAPEVINE_SDL_INPUTKEY

namespace Grapevine
{
	namespace InputKey
	{
		enum eInputKey
		{
			None = 0,

			Quit,

			// mouse input types
			MouseMotion,
			MouseLeft,
			MouseRight,

			// keyboard input types
			A,	B,	C,	D,	E,	F,
			G,	H,	I,	J,	K,	L,
			M,	N,	O,	P,	Q,	R,
			S,	T,	U,	V,	W,	X,
			Y,	Z,

			Zero,	One,	Two,	Three,	Four, 
			Five,	Six,	Seven,	Eight,	Nine,

			Up, Down, Left, Right,

			// Controller Input
		};
	}
}

#endif // GRAPEVINE_SDL_INPUTKEY