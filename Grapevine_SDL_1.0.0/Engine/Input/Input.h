/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include <map>

// engine includes
#include "../../Engine/Input/InputEvent.h"
#include "../../Engine/Input/InputEventGesture.h"
#include "../../Engine/Maths/Vector2.h"

namespace Grapevine
{
	class Input
	{
		friend class Application;

		typedef std::map<InputKey::eInputKey, InputEvent*> InputEventMap;
		typedef std::pair<InputKey::eInputKey, InputEvent*> InputEventMapPair;
		typedef std::map<InputKey::eInputKey, InputEvent*>::iterator	InputEventMapIterator;

	private:
		static Input* GetInstance();
		static void DestroyInstance();

	private:
		Input();
		~Input();

		void UpdateInputEvents();

		InputEvent GetSDLInputAsEngineInput(SDL_Event _event);

		InputKey::eInputKey GetInputKeyFromMouseButtonEvent(SDL_MouseButtonEvent _mouse);
		InputKey::eInputKey GetInputKeyFromKeyboardEvent(SDL_KeyboardEvent _keyboard);

	public:
		static bool Contains(InputKey::eInputKey _inputKey);

		static const Vec2Int& GetMousePosition();

		static const InputEvent& GetInputEvent(InputKey::eInputKey _inputKey);

	private:
		Vec2Int m_mousePosition;

		InputEventMap m_currentEvents;

	private:
		static Input* m_instance;
	};
}