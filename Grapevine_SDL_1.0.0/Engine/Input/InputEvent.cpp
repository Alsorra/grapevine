/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "InputEvent.h"

namespace Grapevine
{
	const InputEvent InputEvent::Empty = InputEvent();

	InputEvent::InputEvent()
		: m_inputKey(InputKey::None)
		, m_inputType(InputType::None)
	{
	}

	InputEvent::InputEvent(InputKey::eInputKey _key, InputType::eInputType _type)
		: m_inputKey(_key)
		, m_inputType(_type)
	{
	}

	InputEvent::InputEvent(const InputEvent& _other)
		: m_inputKey(_other.m_inputKey)
		, m_inputType(_other.m_inputType)
	{
	}
}