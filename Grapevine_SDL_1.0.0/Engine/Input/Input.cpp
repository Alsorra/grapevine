/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

// this class
#include "Input.h"

// engine includes
#include "../../Engine/Input/Time.h"

namespace Grapevine
{
	Input* Input::m_instance = NULL;

	Input* Input::GetInstance()
	{
		if (m_instance == NULL)
		{
			m_instance = new Input();
		}
		return m_instance;
	}

	void Input::DestroyInstance()
	{
		if (m_instance != NULL)
		{
			delete m_instance;
			m_instance = NULL;
		}
	}

	Input::Input()
		: m_mousePosition(Vec2Int(0, 0))
	{
	}
	
	Input::~Input()
	{
		while (m_currentEvents.size() > 0)
		{
			InputEventMapIterator iter = m_currentEvents.begin();

			delete iter->second;
			m_currentEvents.erase(iter);
		}
	}

	void Input::UpdateInputEvents()
	{
		// go through and update all current events
		InputEventMapIterator iter = m_currentEvents.begin();
		while (iter != m_currentEvents.end())
		{
			InputEvent* e = iter->second;

			// pressed events become held events
			if (e->m_inputType == InputType::Pressed)
			{
				e->m_inputType = InputType::Held;
			}

			// these event types are no longer valid
			bool remove = e->m_inputType == InputType::Released
				|| e->m_inputType == InputType::Motion
				|| e->m_inputType == InputType::None;

			if (remove)
			{
				delete iter->second;
				m_currentEvents.erase(iter);
				iter = m_currentEvents.begin();
			}
			else
			{
				++iter;
			}
		}

		SDL_Event e;

		//Handle events on queue
		while( SDL_PollEvent( &e ) != 0 )
		{
			InputEvent input = GetSDLInputAsEngineInput(e);

			if (e.type == SDL_MOUSEMOTION || e.type == SDL_MOUSEBUTTONDOWN || e.type == SDL_MOUSEBUTTONUP)
			{
				int x, y;
				SDL_GetMouseState( &x, &y );

				m_mousePosition = Vec2Int(x, y);
			}

			if (input.m_inputKey == InputKey::None)
			{
				continue;
			}

			InputEventMapIterator it = m_currentEvents.find(input.m_inputKey);
			if (it == m_currentEvents.end())
			{
				if (input.m_inputKey == InputKey::MouseLeft
					|| input.m_inputKey == InputKey::MouseRight)
				{
					InputEventGesture* gesture = new InputEventGesture(input);
					gesture->GestureStart(Time::GetTicks(), m_mousePosition);
					m_currentEvents.insert(InputEventMapPair(input.m_inputKey, gesture));
				}
				else
				{
					m_currentEvents.insert(InputEventMapPair(input.m_inputKey, new InputEvent(input)));
				}
			}
			else
			{
				InputEvent* inputEvent = it->second;
				inputEvent->m_inputType = input.m_inputType;

				if (inputEvent->m_inputType == InputType::Released
					&& (inputEvent->m_inputKey == InputKey::MouseLeft || inputEvent->m_inputKey == InputKey::MouseRight))
				{
					InputEventGesture* gesture = (InputEventGesture*)inputEvent;
					gesture->GestureEnd(Time::GetTicks(), m_mousePosition);
				}
			}
		}
	}

	InputEvent Input::GetSDLInputAsEngineInput(SDL_Event _event)
	{
		InputKey::eInputKey inputKey = InputKey::None;
		InputType::eInputType inputType = InputType::None;

		switch (_event.type)
		{
		case SDL_MOUSEMOTION:
			inputKey = InputKey::MouseMotion;
			inputType = InputType::Motion;
			break;

		case SDL_MOUSEBUTTONDOWN:
			inputKey = GetInputKeyFromMouseButtonEvent(_event.button);
			inputType = InputType::Pressed;
			break;

		case SDL_MOUSEBUTTONUP:
			inputKey = GetInputKeyFromMouseButtonEvent(_event.button);
			inputType = InputType::Released;
			break;

		case SDL_KEYDOWN:
			inputKey = GetInputKeyFromKeyboardEvent(_event.key);
			inputType = InputType::Pressed;
			break;

		case SDL_KEYUP:
			inputKey = GetInputKeyFromKeyboardEvent(_event.key);
			inputType = InputType::Released;
			break;

		case SDL_QUIT:
			inputKey = InputKey::Quit;
			inputType = InputType::Pressed;

		default: break;
		}

		if (inputKey != InputKey::None && inputType != InputType::None)
		{
			return InputEvent(inputKey, inputType);
		}

		return InputEvent::Empty;
	}

	InputKey::eInputKey Input::GetInputKeyFromMouseButtonEvent(SDL_MouseButtonEvent _mouse)
	{
		if (_mouse.button == 1)
		{
			return InputKey::MouseLeft;
		}
		else if (_mouse.button == 3)
		{
			return InputKey::MouseRight;
		}

		return InputKey::None;
	}
	InputKey::eInputKey Input::GetInputKeyFromKeyboardEvent(SDL_KeyboardEvent _keyboard)
	{
		switch (_keyboard.keysym.sym)
		{
		case SDLK_a: return InputKey::A;
		case SDLK_b: return InputKey::B;
		case SDLK_c: return InputKey::C;
		case SDLK_d: return InputKey::D;
		case SDLK_e: return InputKey::E;
		case SDLK_f: return InputKey::F;
		case SDLK_g: return InputKey::G;
		case SDLK_h: return InputKey::H;
		case SDLK_i: return InputKey::I;
		case SDLK_j: return InputKey::J;
		case SDLK_k: return InputKey::K;
		case SDLK_l: return InputKey::L;
		case SDLK_m: return InputKey::M;
		case SDLK_n: return InputKey::N;
		case SDLK_o: return InputKey::O;
		case SDLK_p: return InputKey::P;
		case SDLK_q: return InputKey::Q;
		case SDLK_r: return InputKey::R;
		case SDLK_s: return InputKey::S;
		case SDLK_t: return InputKey::T;
		case SDLK_u: return InputKey::U;
		case SDLK_v: return InputKey::V;
		case SDLK_w: return InputKey::W;
		case SDLK_x: return InputKey::X;
		case SDLK_y: return InputKey::Y;
		case SDLK_z: return InputKey::Z;

		case SDLK_0: return InputKey::Zero;
		case SDLK_1: return InputKey::One;
		case SDLK_2: return InputKey::Two;
		case SDLK_3: return InputKey::Three;
		case SDLK_4: return InputKey::Four;
		case SDLK_5: return InputKey::Five;
		case SDLK_6: return InputKey::Six;
		case SDLK_7: return InputKey::Seven;
		case SDLK_8: return InputKey::Eight;
		case SDLK_9: return InputKey::Nine;

		case SDLK_UP:		return InputKey::Up;
		case SDLK_DOWN:		return InputKey::Down;
		case SDLK_LEFT:		return InputKey::Left;
		case SDLK_RIGHT:	return InputKey::Right;

		default: break;
		}

		return InputKey::None;
	}

	bool Input::Contains(InputKey::eInputKey _inputKey)
	{
		Input* instance = GetInstance();

		InputEventMapIterator it = instance->m_currentEvents.find(_inputKey);

		return it != instance->m_currentEvents.end();
	}

	const InputEvent& Input::GetInputEvent(InputKey::eInputKey _inputKey)
	{
		Input* instance = GetInstance();

		InputEventMapIterator it = instance->m_currentEvents.find(_inputKey);
		if (it != instance->m_currentEvents.end())
		{
			return *it->second;
		}

		return InputEvent::Empty;
	}

	const Vec2Int& Input::GetMousePosition()
	{
		return GetInstance()->m_mousePosition;
	}
}