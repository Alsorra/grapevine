/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "InputEventGesture.h"

#define CLICKMAXTIME 200

namespace Grapevine
{
	InputEventGesture::GestureStamp::GestureStamp()
		: m_timestamp(0)
		, m_mousePosition(Vec2Int(0, 0))
	{}

	InputEventGesture::InputEventGesture()
		: InputEvent()
		, m_gestureType(GestureType::None)
	{}

	InputEventGesture::InputEventGesture(InputKey::eInputKey _key)
		: InputEvent(_key, InputType::Gesture)
		, m_gestureType(GestureType::None)
	{}

	InputEventGesture::InputEventGesture(const InputEvent& _other)
		: InputEvent(_other)
		, m_gestureType(GestureType::None)
	{}

	InputEventGesture::InputEventGesture(const InputEventGesture& _other)
		: InputEvent(_other)
		, m_gestureType(_other.m_gestureType)
	{}

	void InputEventGesture::UpdateGesture(unsigned _timestamp, const Vec2Int& _mousePosition)
	{
		if (_timestamp - m_gestureStart.m_timestamp < CLICKMAXTIME)
		{
			m_gestureType = GestureType::Drag;
		}
	}
	
	void InputEventGesture::GestureStart(unsigned _timestamp, const Vec2Int& _mousePosition)
	{
		m_gestureStart.m_timestamp = _timestamp;
		m_gestureStart.m_mousePosition = _mousePosition;
	}

	void InputEventGesture::GestureEnd(unsigned _timestamp, const Vec2Int& _mousePosition)
	{
		m_gestureEnd.m_timestamp = _timestamp;
		m_gestureEnd.m_mousePosition = _mousePosition;
		
		m_gestureType = (m_gestureEnd.m_timestamp - m_gestureStart.m_timestamp < CLICKMAXTIME) ?
			GestureType::Click :
			GestureType::Drag;
	}

	GestureType::eGestureType InputEventGesture::GetGestureType() const
	{
		return m_gestureType;
	}

	const Vec2Int& InputEventGesture::GetGestureStartMousePosition() const
	{
		return m_gestureStart.m_mousePosition;
	}

	const Vec2Int& InputEventGesture::GetGestureEndMousePosition() const
	{
		return m_gestureEnd.m_mousePosition;
	}
}