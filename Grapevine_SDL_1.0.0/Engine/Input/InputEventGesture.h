/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_INPUTEVENTGESTURE
#define GRAPEVINE_SDL_INPUTEVENTGESTURE

// engine includes
#include "../../Engine/Input/InputEvent.h"
#include "../../Engine/Maths/Vector2.h"

namespace Grapevine
{
	class InputEventGesture : public InputEvent
	{
	private:
		struct GestureStamp
		{
			GestureStamp();

			unsigned m_timestamp;

			Vec2Int m_mousePosition;
		};

	public:
		InputEventGesture();

		InputEventGesture(InputKey::eInputKey _key);

		InputEventGesture(const InputEvent& _other);

		InputEventGesture(const InputEventGesture& _other);

		void UpdateGesture(unsigned _timestamp, const Vec2Int& _mousePosition);
		
		void GestureStart(unsigned _timestamp, const Vec2Int& _mousePosition);

		void GestureEnd(unsigned _timestamp, const Vec2Int& _mousePosition);

		GestureType::eGestureType GetGestureType() const;

		const Vec2Int& GetGestureStartMousePosition() const;

		const Vec2Int& GetGestureEndMousePosition() const;

	private:
		GestureType::eGestureType m_gestureType;

		GestureStamp m_gestureStart;

		GestureStamp m_gestureEnd;
	};
}

#endif // GRAPEVINE_SDL_INPUTEVENTGESTURE