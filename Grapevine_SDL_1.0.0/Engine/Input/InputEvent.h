/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_INPUTEVENT
#define GRAPEVINE_SDL_INPUTEVENT

#include <SDL.h>

// engine includes
#include "InputKey.h"
#include "InputType.h"

namespace Grapevine
{
	class InputEvent
	{
	public:
		InputEvent();
		InputEvent(InputKey::eInputKey _key, InputType::eInputType _type);
		InputEvent(const InputEvent& _other);

		bool operator==(const InputEvent& _other) const;

	public:
		const InputKey::eInputKey m_inputKey;

		InputType::eInputType m_inputType;

	public:
		static const InputEvent Empty;
	};
}

#endif // GRAPEVINE_SDL_INPUTEVENT