/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_INPUTTYPE
#define GRAPEVINE_SDL_INPUTTYPE

namespace Grapevine
{
	namespace InputType
	{
		enum eInputType
		{
			None = 0,

			Pressed,
			Held,
			Released,

			Motion,
			Gesture
		};
	}

	namespace GestureType
	{
		enum eGestureType
		{
			None = 0,

			Click,
			Drag
		};
	}
}

#endif // GRAPEVINE_SDL_INPUTTYPE