/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

namespace Grapevine
{
	class Time
	{
		friend class Application;

	private:
		static Time* GetInstance();
		static void DestroyInstance();

	private:
		Time();
		~Time();

		static void CalculateDeltaTime();

	public:
		//! Get time since program start
		static unsigned GetTicks();

		static unsigned GetDeltaTime();

	private:
		unsigned m_deltaTime;

		unsigned m_lastFrameTime;

	private:
		static Time* m_instance;
	};
}