/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Time.h"

#include "../Maths/Maths.h"

#include <SDL.h>

namespace Grapevine
{
	Time* Time::m_instance = nullptr;

	Time* Time::GetInstance()
	{
		if (m_instance == nullptr)
		{
			m_instance = new Time();
		}
		return m_instance;
	}

	void Time::DestroyInstance()
	{
		if (m_instance != nullptr)
		{
			delete m_instance;
			m_instance = nullptr;
		}
	}

	Time::Time()
		: m_deltaTime(0)
		, m_lastFrameTime(0)
	{
	}
	
	Time::~Time()
	{
	}

	void Time::CalculateDeltaTime()
	{
		unsigned sinceStart = SDL_GetTicks();

		GetInstance()->m_deltaTime = Maths::Min<unsigned>(sinceStart - GetInstance()->m_lastFrameTime, 100);

		GetInstance()->m_lastFrameTime = sinceStart;
	}

	unsigned Time::GetTicks()
	{
		return SDL_GetTicks();
	}

	unsigned Time::GetDeltaTime()
	{
		return GetInstance()->m_deltaTime;
	}
}