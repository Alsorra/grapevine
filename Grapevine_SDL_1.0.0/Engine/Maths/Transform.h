/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_TRANSFORM
#define GRAPEVINE_SDL_TRANSFORM

#include "../Maths/Vector2.h"
#include "../Maths/Matrix3x3.h"

namespace Grapevine
{
	class Transform 
	{
	public:
		Transform();

		Transform(const Vec2& _translation);

		Transform(const Vec2& _translation, const Vec2& _scale);

		Transform(const Vec2& _translation, const Vec2& _scale, float _rotation);

		Transform(const Transform& _other);

		virtual ~Transform();

		void Clear();

		Matrix3x3 GetMatrix() const;

		void operator+=(const Transform& _other);

		void operator-=(const Transform& _other);

		Transform operator+(const Transform& _other) const;

		Transform operator-(const Transform& _other) const;

		Transform operator*(const float& _float) const;

		bool operator==(const Transform& _other) const;

		bool operator!=(const Transform& _other) const;

	public:
		void Translate(const Vec2& _translate);

		void SetTranslation(const Vec2& _translation);

		void Scale(const Vec2& _scale);

		void SetScale(const Vec2& _scale);

		void Rotate(float _degrees);

		void SetRotation(float _degrees);

	public:
		const Vec2& GetTranslation() const;

		const Vec2& GetScale() const;

		float GetRotation() const;

	private:
		float m_rotation;

		Vec2 m_translation;

		Vec2 m_scale;
	};
}

#endif // GRAPEVINE_SDL_TRANSFORM