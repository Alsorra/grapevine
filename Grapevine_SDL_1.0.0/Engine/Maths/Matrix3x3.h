/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_MATRIX3X3
#define GRAPEVINE_SDL_MATRIX3X3

#include "Vector2.h"

namespace Grapevine
{
	class Matrix3x3
	{
	public:
		Matrix3x3();
		Matrix3x3(const Matrix3x3& _other);
		Matrix3x3(float _11, float _12, float _13,
			float _21, float _22, float _23,
			float _31, float _32, float _33);

		Vec2 TransformPoint(const Vec2& _point);

	public:
		void SetTranslation(const Vec2& _translation);
		void SetScale(const Vec2& _scale);
		void SetRotation(float _rotation);

	private:
		float a1, a2, a3;
		float b1, b2, b3;
		float c1, c2, c3;

	public:
		static const Matrix3x3 Identity;

	public:
		Matrix3x3 operator*(const Matrix3x3& _other) const;
		Matrix3x3 operator/(const Matrix3x3& _other) const;
	};
}

#endif // GRAPEVINE_SDL_MATRIX3X3