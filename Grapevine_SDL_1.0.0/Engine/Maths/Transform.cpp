/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Transform.h"

namespace Grapevine
{
	Transform::Transform()
		: m_rotation(0.0f)
		, m_translation(Vec2(0.0f, 0.0f))
		, m_scale(Vec2(1.0f, 1.0f))
	{}

	Transform::Transform(const Vec2& _translation)
		: m_rotation(0.0f)
		, m_translation(_translation)
		, m_scale(Vec2(1.0f, 1.0f))
	{}

	Transform::Transform(const Vec2& _translation, const Vec2& _scale)
		: m_rotation(0.0f)
		, m_translation(_translation)
		, m_scale(_scale)
	{}

	Transform::Transform(const Vec2& _translation, const Vec2& _scale, float _rotation)
		: m_rotation(_rotation)
		, m_translation(_translation)
		, m_scale(_scale)
	{}

	Transform::Transform(const Transform& _other)
		: m_rotation(_other.m_rotation)
		, m_translation(_other.m_translation)
		, m_scale(_other.m_scale)
	{}

	Transform::~Transform()
	{}

	void Transform::Clear()
	{
		m_rotation = 0.0f;
		m_translation = Vec2(0.0f, 0.0f);
		m_scale = Vec2(0.0f, 0.0f);
	}

	Matrix3x3 Transform::GetMatrix() const
	{
		Matrix3x3 t;
		Matrix3x3 s;
		Matrix3x3 r;

		t.SetTranslation(this->m_translation);
		s.SetScale(this->m_scale);
		r.SetRotation(this->m_rotation);

		Matrix3x3 matrix = t * r * s;

		return matrix;
	}

	void Transform::operator+=(const Transform& _other)
	{
		m_rotation += _other.m_rotation;
		m_translation += _other.m_translation;
		m_scale *= _other.m_scale;
	}

	void Transform::operator-=(const Transform& _other)
	{
		m_rotation -= _other.m_rotation;
		m_translation -= _other.m_translation;
		m_scale /= _other.m_scale;
	}

	Transform Transform::operator+(const Transform& _other) const
	{
		Transform transform;

		transform.m_rotation = m_rotation + _other.m_rotation;
		transform.m_translation = m_translation + _other.m_translation;
		transform.m_scale = m_scale * _other.m_scale;

		return transform;
	}

	Transform Transform::operator-(const Transform& _other) const
	{
		Transform transform;

		transform.m_rotation = m_rotation - _other.m_rotation;
		transform.m_translation = m_translation - _other.m_translation;
		transform.m_scale = m_scale / _other.m_scale;

		return transform;
	}

	Transform Transform::operator*(const float& _float) const
	{
		Transform transform;

		transform.m_rotation = m_rotation * _float;
		transform.m_translation = m_translation * _float;
		transform.m_scale = m_scale * _float;

		return transform;
	}

	bool Transform::operator==(const Transform& _other) const
	{
		return m_rotation == _other.m_rotation
			&& m_translation == _other.m_translation
			&& m_scale == _other.m_scale;
	}

	bool Transform::operator!=(const Transform& _other) const
	{
		return m_rotation != _other.m_rotation
			|| m_translation != _other.m_translation
			|| m_scale != _other.m_scale;
	}

	void Transform::Translate(const Vec2& _translate)
	{ 
		m_translation += _translate; 
	}

	void Transform::SetTranslation(const Vec2& _translation)
	{ 
		m_translation = _translation; 
	}

	void Transform::Scale(const Vec2& _scale)
	{ 
		m_scale += _scale; 
	}

	void Transform::SetScale(const Vec2& _scale)
	{ 
		m_scale = _scale; 
	}

	void Transform::Rotate(float _degrees) 
	{
		m_rotation += _degrees; 
	}

	void Transform::SetRotation(float _degrees) 
	{ 
		m_rotation = _degrees; 
	}

	const Vec2& Transform::GetTranslation() const
	{ 
		return m_translation; 
	}

	const Vec2& Transform::GetScale() const
	{ 
		return m_scale; 
	}

	float Transform::GetRotation() const
	{ 
		return m_rotation; 
	}
}