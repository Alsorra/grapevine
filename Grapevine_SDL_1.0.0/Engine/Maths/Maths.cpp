/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Maths.h"

#include <math.h>

namespace Grapevine
{
	float Maths::Sin(float _radians)
	{
		return (float)sin((double)_radians);
	}

	float Maths::Cos(float _radians)
	{
		return (float)cos((double)_radians);
	}

	float Maths::Tan(float _radians)
	{
		return (float)tan((double)_radians);
	}

	float Maths::Abs(float _value)
	{
		return _value < 0.0f ? _value * -1.0f : _value;
	}

	int Maths::Abs(int _value)
	{
		return _value < 0 ? _value * -1 : _value; 
	}
}