/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Matrix3x3.h"

#include "Maths.h"

namespace Grapevine
{
	const Matrix3x3 Matrix3x3::Identity = Matrix3x3();

	Matrix3x3::Matrix3x3()
		: a1(1), a2(0), a3(0)
		, b1(0), b2(1), b3(0)
		, c1(0), c2(0), c3(1)
	{
	}

	Matrix3x3::Matrix3x3(const Matrix3x3& _other)
		: a1(_other.a1), a2(_other.a2), a3(_other.a3)
		, b1(_other.b1), b2(_other.b2), b3(_other.b3)
		, c1(_other.c1), c2(_other.c2), c3(_other.c3)
	{
	}

	Matrix3x3::Matrix3x3(float _11, float _12, float _13, 
						 float _21, float _22, float _23,
						 float _31, float _32, float _33)
		: a1(_11), a2(_12), a3(_13)
		, b1(_11), b2(_12), b3(_13)
		, c1(_11), c2(_12), c3(_13)
	{
	}

	Vec2 Matrix3x3::TransformPoint(const Vec2& _point)
	{
		Vec2 point;

		point.x = (a1*_point.x) + (a2*_point.y);// + (a3);
		point.y = (b1*_point.x) + (b2*_point.y);// + (b3);

		return point;
	}

	void Matrix3x3::SetTranslation(const Vec2& _translation)
	{
		*this = Matrix3x3::Identity;

		a3 = _translation.x;
		b3 = _translation.y;
	}
	void Matrix3x3::SetScale(const Vec2& _scale)
	{
		*this = Matrix3x3::Identity;

		a1 = _scale.x;
		b2 = _scale.y;
	}
	void Matrix3x3::SetRotation(float _rotation)
	{
		*this = Matrix3x3::Identity;

		float radians = DEG_TO_RAD(_rotation);

		a1 = Maths::Cos(radians);
		a2 = -Maths::Sin(radians);
		b1 = Maths::Sin(radians);
		b2 = Maths::Cos(radians);
	}

	Matrix3x3 Matrix3x3::operator*(const Matrix3x3& _other) const
	{
		Matrix3x3 matrix;

		matrix.a1 = (a1*_other.a1) + (a2*_other.b1) + (a3*_other.c1);
		matrix.a2 = (a1*_other.a2) + (a2*_other.b2) + (a3*_other.c2);
		matrix.a3 = (a1*_other.a3) + (a2*_other.b3) + (a3*_other.c3);
		matrix.b1 = (b1*_other.a1) + (b2*_other.b1) + (b3*_other.c1);
		matrix.b2 = (b1*_other.a2) + (b2*_other.b2) + (b3*_other.c2);
		matrix.b3 = (b1*_other.a3) + (b2*_other.b3) + (b3*_other.c3);
		matrix.c1 = (c1*_other.a1) + (c2*_other.b1) + (c3*_other.c1);
		matrix.c2 = (c1*_other.a2) + (c2*_other.b2) + (c3*_other.c2);
		matrix.c3 = (c1*_other.a3) + (c2*_other.b3) + (c3*_other.c3);

		return matrix;
	}

	Matrix3x3 Matrix3x3::operator/(const Matrix3x3& _other) const
	{
		Matrix3x3 matrix;

		return matrix;
	}
}