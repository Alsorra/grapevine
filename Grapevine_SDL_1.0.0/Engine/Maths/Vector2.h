/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

namespace Grapevine
{
	template<class T>
	struct Vector2
	{
	public:
		Vector2<T>();
		Vector2<T>(const T& _x, const T& _y);
		Vector2<T>(const Vector2<T>& _vector);

		bool operator==(const Vector2<T>& _other) const;
		bool operator!=(const Vector2<T>& _other) const;

		void operator=(const Vector2<T>& _other);
		void operator+=(const Vector2<T>& _other);
		void operator-=(const Vector2<T>& _other);
		void operator*=(const Vector2<T>& _other);
		void operator/=(const Vector2<T>& _other);

		Vector2<T> operator+(const Vector2<T>& _other) const;
		Vector2<T> operator-(const Vector2<T>& _other) const;
		Vector2<T> operator*(const Vector2<T>& _other) const;
		Vector2<T> operator/(const Vector2<T>& _other) const;

		Vector2<T> operator*(const T& _other) const;

	public:
		T x;
		T y;
	};

	template<class T>
	Vector2<T>::Vector2()
	{}

	template<class T>
	Vector2<T>::Vector2(const T& _x, const T& _y)
		: x(_x)
		, y(_y)
	{}

	template<class T>
	Vector2<T>::Vector2(const Vector2<T>& _other)
		: x(_other.x)
		, y(_other.y)
	{}

	template<class T>
	bool Vector2<T>::operator==(const Vector2<T>& _other) const
	{
		return x == _other.x
			&& y == _other.y;
	}

	template<class T>
	bool Vector2<T>::operator!=(const Vector2<T>& _other) const
	{
		return x != _other.x
			&& y != _other.y;
	}

	template<class T>
	void Vector2<T>::operator=(const Vector2<T>& _other)
	{
		x = _other.x;
		y = _other.y;
	}

	template<class T>
	void Vector2<T>::operator+=(const Vector2<T>& _other)
	{
		x += _other.x;
		y += _other.y;
	}

	template<class T>
	void Vector2<T>::operator-=(const Vector2<T>& _other)
	{
		x -= _other.x;
		y -= _other.y;
	}

	template<class T>
	void Vector2<T>::operator*=(const Vector2<T>& _other)
	{
		x *= _other.x;
		y *= _other.y;
	}

	template<class T>
	void Vector2<T>::operator/=(const Vector2<T>& _other)
	{
		x /= _other.x;
		y /= _other.y;
	}

	template<class T>
	Vector2<T> Vector2<T>::operator+(const Vector2<T>& _other) const
	{
		Vector2<T> vec;
		vec.x = x + _other.x;
		vec.y = y + _other.y;

		return vec;
	}

	template<class T>
	Vector2<T> Vector2<T>::operator-(const Vector2<T>& _other) const
	{
		Vector2<T> vec;
		vec.x = x - _other.x;
		vec.y = y - _other.y;

		return vec;
	}

	template<class T>
	Vector2<T> Vector2<T>::operator*(const Vector2<T>& _other) const
	{
		Vector2<T> vec;
		vec.x = x * _other.x;
		vec.y = y * _other.y;

		return vec;
	}

	template<class T>
	Vector2<T> Vector2<T>::operator/(const Vector2<T>& _other) const
	{
		Vector2<T> vec;
		vec.x = x / _other.x;
		vec.y = y / _other.y;

		return vec;
	}

	template<class T>
	Vector2<T> Vector2<T>::operator*(const T& _other) const
	{
		Vector2<T> vec;
		vec.x = x * _other;
		vec.y = y * _other;

		return vec;
	}
}

typedef Grapevine::Vector2<float> Vec2;
typedef Grapevine::Vector2<int> Vec2Int;