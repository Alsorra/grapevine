/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_MATHS
#define GRAPEVINE_SDL_MATHS

#define PI 3.14159265359f

#define DEG_TO_RAD(a) (a * PI) / 180.0f
#define RAD_TO_DEG(a) (a * 180.0f) / PI

namespace Grapevine
{
	class Maths
	{
	public:
		static float Sin(float _radians);

		static float Cos(float _radians);

		static float Tan(float _radians);

		static float Abs(float _value);

		static int Abs(int _value);

		template <class T>
		static T Min(const T& _first, const T& _second);

		template <class T>
		static T Max(const T& _first, const T& _second);
	};

	template <class T>
	T Maths::Min(const T& _first, const T& _second)
	{
		return _first < _second ? _first : _second;
	}

	template <class T>
	T Maths::Max(const T& _first, const T& _second)
	{
		return _first > _second ? _first : _second;
	}
}

#endif // GRAPEVINE_SDL_MATHS