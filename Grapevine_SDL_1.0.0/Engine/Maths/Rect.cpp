/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Rect.h"

namespace Grapevine
{
	Rect::Rect()
		: left(0)
		, right(0)
		, top(0)
		, bottom(0)
	{
	}
	Rect::Rect(int _left, int _right, int _top, int _bottom)
		: left(_left)
		, right(_right)
		, top(_top)
		, bottom(_bottom)
	{
	}
	Rect::Rect(const Vec2& _topLeft, int _width, int _height)
		: left((int)_topLeft.x)
		, right((int)_topLeft.x + _width)
		, top((int)_topLeft.y)
		, bottom((int)_topLeft.y + _height)
	{
	}
	Rect::Rect(const Rect& _other)
		: left(_other.left)
		, right(_other.right)
		, top(_other.top)
		, bottom(_other.bottom)
	{
	}

	int Rect::GetWidth() const
	{
		return right - left;
	}

	int Rect::GetHeight() const
	{
		return bottom - top;
	}

	bool Rect::operator==(const Rect& _other)
	{
		return _other.left == left && _other.right == right && _other.top == top && _other.bottom == bottom;
	}
	bool Rect::operator!=(const Rect& _other)
	{
		return _other.left != left || _other.right != right || _other.top != top || _other.bottom != bottom;
	}

	void Rect::operator=(const Rect& _other)
	{
		left = _other.left;
		right = _other.right;
		top = _other.top;
		bottom = _other.bottom;
	}

	bool Rect::IntersectionOccurs(const Rect& _other) const
	{
		return left < _other.right
			&& right > _other.left
			&& top < _other.bottom
			&& bottom > _other.top;
	}
}