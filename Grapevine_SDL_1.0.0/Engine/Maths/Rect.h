/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include "Vector2.h"

namespace Grapevine
{
	struct Rect
	{
	public:
		Rect();
		Rect(int _left, int _right, int _top, int _bottom);
		Rect(const Vec2& _topLeft, int _width, int _height);
		Rect(const Rect& _other);

		int GetWidth() const;
		int GetHeight() const;

		bool operator==(const Rect& _other);
		bool operator!=(const Rect& _other);

		void operator=(const Rect& _other);

		bool IntersectionOccurs(const Rect& _other) const;

	public:
		int left;
		int right;
		int top;
		int bottom;
	};
}