/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include <vector>

namespace Grapevine
{
	template<class T>
	class Speaker
	{
	public:
		virtual ~Speaker() = default;

		void AddListener(T* listener)
		{
			if (listener != nullptr)
			{
				mListeners.push_back(listener);
			}
		}

		void RemoveListener(T* listener)
		{
			const auto& iter = std::find(mListeners.begin(), mListeners.end(), listener);
			if (iter != mListeners.end())
			{
				mListeners.erase(iter);
			}
		}

	protected:
		const std::vector<T*> GetListeners() const { return mListeners; }

	private:
		std::vector<T*> mListeners;
	};
}