/*
*
* Grapevine SDL 1.0.0
*
* Games Engine
*
* Written and Owned by Alice Thorntonsmith
*
*/

#ifndef GRAPEVINE_SDL_JSONHELPER
#define GRAPEVINE_SDL_JSONHELPER

#include "../../Engine/Json/json.h"
#include "../../Engine/Maths/Transform.h"

namespace Grapevine
{
	class JsonHelper
	{
	public:
		static bool TryLoadJsonFromFile(const std::string& _file, Json::Value& _json);

		static bool TrySaveJsonToFile(const std::string& file, const Json::Value& _json);

	public:
		static Vec2 LoadVec2FromJson(const Json::Value& _json);

		static Transform LoadTransformFromJson(const Json::Value& _json);
	};
}

#endif // GRAPEVINE_SDL_JSONHELPER