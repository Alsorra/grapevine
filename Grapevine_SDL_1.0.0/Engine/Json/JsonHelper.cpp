/*
*
* Grapevine SDL 1.0.0
*
* Games Engine
*
* Written and Owned by Alice Thorntonsmith
*
*/

#include "JsonHelper.h"

#include <fstream>

namespace Grapevine
{
	bool JsonHelper::TryLoadJsonFromFile(const std::string& _file, Json::Value& _json)
	{
		std::ifstream file(_file);
		std::string content((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

		Json::Reader reader;
		bool success = reader.parse(content, _json);

		file.close();

		return success;
	}

	bool JsonHelper::TrySaveJsonToFile(const std::string& file, const Json::Value& _json)
	{
		// TODO
		return false;
	}

	Vec2 JsonHelper::LoadVec2FromJson(const Json::Value& _json)
	{
		if (_json.isArray())
		{
			float x = _json[0].isNumeric() ? _json[0].asFloat() : 0.0f;
			float y = _json[1].isNumeric() ? _json[1].asFloat() : 0.0f;

			return Vec2(x, y);
		}

		return Vec2(0.0f, 0.0f);
	}

	Transform JsonHelper::LoadTransformFromJson(const Json::Value& _json)
	{
		Transform transform;

		transform.SetTranslation(LoadVec2FromJson(_json["translation"]));
		transform.SetScale(LoadVec2FromJson(_json["scale"]));
		transform.SetRotation(_json["rotation"].isNumeric() ? _json["rotation"].asFloat() : 0.0f);

		return transform;
	}
}