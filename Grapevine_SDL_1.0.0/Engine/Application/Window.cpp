/*
 *
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

#include "Window.h"

#include "../Application/Renderer.h"
#include "../Components/Physics/PhysicsWorldComponent.h"

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

namespace Grapevine
{
	Window::Window(const int width, const int height)
		: mSDLWindow(nullptr)
	{
		//Initialize SDL
		if (SDL_Init(SDL_INIT_VIDEO) < 0)
		{
			printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		}
		else
		{
			//Set texture filtering to linear
			if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
			{
				printf("Warning: Linear texture filtering not enabled!");
			}

			//Create window
			mSDLWindow = SDL_CreateWindow("Grapevine", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN);
			if (mSDLWindow == nullptr)
			{
				printf("Window could not be created! SDL Error: %s\n", SDL_GetError());
			}
			else
			{
				//Create renderer for window
				Renderer::GetInstance()->m_renderer = SDL_CreateRenderer(mSDLWindow, -1, SDL_RENDERER_ACCELERATED);

				SDL_Renderer* renderer = Renderer::GetInstance()->m_renderer;

				if (renderer == nullptr)
				{
					printf("Renderer could not be created! SDL Error: %s\n", SDL_GetError());
				}
				else
				{
					//Initialize renderer color
					SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);

					//Initialize PNG loading
					int imgFlags = IMG_INIT_PNG;
					if (!(IMG_Init(imgFlags) & imgFlags))
					{
						printf("SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError());
					}

					//Initialize SDL_ttf
					if (TTF_Init() == -1)
					{
						printf("SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError());
					}
				}
			}
		}
	}

	Window::~Window()
	{
		Renderer::DestroyInstance();

		//Destroy window	
		SDL_DestroyWindow(mSDLWindow);
		mSDLWindow = nullptr;

		//Quit SDL subsystems
		IMG_Quit();
		SDL_Quit();
	}

	void Window::UpdateAndRenderWindow()
	{
		mMainObject.Update();

		mMainObject.GetPhysicsWorldComponent()->UpdatePhysics();

		Renderer::GetInstance()->RenderScreen(mMainObject);
	}
}