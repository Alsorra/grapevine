/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include "../Objects/World.h"

struct SDL_Window;

namespace Grapevine
{
	class Window
	{
	public:
		Window(const int width, const int height);
		~Window();

		void UpdateAndRenderWindow();

		World& GetMainObject() { return mMainObject; }

	private:
		SDL_Window* mSDLWindow;

		World mMainObject;
	};
}