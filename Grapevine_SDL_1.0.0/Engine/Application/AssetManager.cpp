/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

// this class
#include "AssetManager.h"

// required includes
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <string>

// engine includes
#include "../../Engine/Application/Renderer.h"
#include "../../Engine/Components/Renderers/TextRenderer.h"
#include "../../Engine/Graphics/Texture.h"
#include "../../Engine/Graphics/TrueTypeFont.h"

namespace Grapevine
{
	void AssetManager::GenerateFontTexture(TextRenderer* _renderer)
	{
		_renderer->m_texture.Reset();

		if (_renderer->m_font == nullptr)
		{
			return;
		}

		//Render text surface
		SDL_Color textColour = { _renderer->m_colour.r, _renderer->m_colour.g, _renderer->m_colour.b };
		SDL_Surface* textSurface = TTF_RenderText_Solid(_renderer->m_font->GetTTFFont(), _renderer->m_text.c_str(), textColour);
		if (textSurface == nullptr)
		{
			printf("Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError());
			return;
		}

		//Create texture from surface pixels
		_renderer->m_texture.m_sdlTexture = SDL_CreateTextureFromSurface(Renderer::GetInstance()->m_renderer, textSurface);
		if (_renderer->m_texture.m_sdlTexture == nullptr)
		{
			printf("Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError());
		}
		else
		{
			//Get image dimensions
			_renderer->m_texture.m_width = textSurface->w;
			_renderer->m_texture.m_height = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface(textSurface);
	}


	// ---

	AssetManager::AssetManager(const std::string& _folderLocation)
		: FolderLocation(_folderLocation)
	{
	}

	AssetManager::~AssetManager()
	{
		AssetMapIterator iter = m_assets.begin();
		while (!m_assets.empty())
		{
			iter = m_assets.begin();

			Asset* asset = iter->second;

			m_assets.erase(iter);

			delete asset;
		}
	}

	void AssetManager::SetupAsset(const std::string& _fileName, unsigned _key)
	{
		m_keys.insert(KeyMapPair(_fileName, _key));
	}

	bool AssetManager::LoadAssets()
	{
		SetupAssets();

		KeyMapConstIterator iter = this->m_keys.begin();
		while (iter != m_keys.end())
		{
			const std::string& file = iter->first;
			unsigned key = iter->second;

			std::string path = FolderLocation.c_str();
			path += "\\";
			path += file.c_str();

			eAssetType type = GetAssetType(file);

			bool loaded = false;

			if (type == eAssetType_Texture)
			{
				loaded = LoadTexture(key, path);
			}
			else if (type == eAssetType_FontTexture)
			{
				loaded = LoadFontTexture(key, path);
			}

			if (!loaded)
			{
				return false;
			}

			++iter;
		}

		return true;
	}

	bool AssetManager::GetAssetKey(const std::string& _file, unsigned& _key) const
	{
		KeyMapConstIterator iter = m_keys.find(_file);

		if (iter != m_keys.end())
		{
			_key = iter->second;
			return true;
		}

		return false;
	}

	bool AssetManager::LoadTexture(unsigned _id, const std::string& _path)
	{
		if (m_assets.find(_id) != m_assets.end())
		{
			// already loaded
			return false;
		}

		SDL_Texture* sdlTexture = nullptr;

		unsigned _int16 width = 0;
		unsigned _int16 height = 0;

		//Load image at specified path
		SDL_Surface* loadedSurface = IMG_Load(_path.c_str());
		if (loadedSurface == nullptr)
		{
			printf( "Unable to load image %s! SDL_image Error: %s\n", _path.c_str(), IMG_GetError() );
			return false;
		}
		else
		{
			//Color key image
			SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

			//Create texture from surface pixels
			sdlTexture = SDL_CreateTextureFromSurface( Renderer::GetInstance()->m_renderer, loadedSurface );
			if (sdlTexture == nullptr)
			{
				printf( "Unable to create texture from %s! SDL Error: %s\n", _path.c_str(), SDL_GetError() );
				return false;
			}
			else
			{
				//Get image dimensions
				width = loadedSurface->w;
				height = loadedSurface->h;
			}

			//Get rid of old loaded surface
			SDL_FreeSurface( loadedSurface );
		}

		Texture* texture = new Texture();

		texture->m_sdlTexture = sdlTexture;
		texture->m_assetId = _id;
		texture->m_u = 0;
		texture->m_v = 0;
		texture->m_width = width;
		texture->m_height = height;

		m_assets.insert(AssetMapPair(_id, texture));

		return true;
	}

	bool AssetManager::LoadFontTexture(unsigned _id, const std::string& _path)
	{
		if (m_assets.find(_id) != m_assets.end())
		{
			// already loaded
			return false;
		}

		//Load image at specified path
		TTF_Font* font = TTF_OpenFont(_path.c_str(), 24);
		if (font == nullptr)
		{
			printf("Failed to load font! SDL_ttf Error: %s\n", TTF_GetError());
			return false;
		}

		TrueTypeFont* ttf = new TrueTypeFont();

		ttf->m_ttfFont = font;
		ttf->m_assetId = _id;

		m_assets.insert(AssetMapPair(_id, ttf));

		return true;
	}

	AssetManager::eAssetType AssetManager::GetAssetType(const std::string& _file) const
	{
		std::string temp;

		for (int i = _file.size() - 1; i >= 0; --i)
		{
			char c = _file[i];
			if (c == '.')
			{
				break;
			}
			temp += _file[i];
		}

		if (temp == "gnp")
		{
			return AssetManager::eAssetType_Texture;
		}
		else if (temp == "ftt")
		{
			return AssetManager::eAssetType_FontTexture;
		}

		return AssetManager::eAssetType_None;
	}
}