/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_RENDERER
#define GRAPEVINE_SDL_RENDERER

// source includes
#include "../Graphics/Colour.h"
#include "../Maths/Matrix3x3.h"
#include "../Maths/Rect.h"
#include "../Maths/Transform.h"
#include "../Maths/Vector2.h"

#include <stack>

struct SDL_Renderer;

namespace Grapevine
{
	class Texture;
	class World;

	class Renderer
	{
		friend class AssetManager;
		friend class Window;

	private:
		static Renderer* GetInstance();
		static void DestroyInstance();

	private:
		Renderer();
		~Renderer();

	private:
		void RenderScreen(World& world);

	public:
		static void PushTransform(const Transform& _transform);

		static void PopTransform();

		static void ClearTransform();

	private:
		static Vec2 TransformPoint(const Vec2& _point);

		static Rect TransformRect(const Rect& _rect);

	public:
		static void RenderPixel(const Vec2& _pixel, const Colour& _colour = Colour::Black);

		static void RenderLine(const Vec2& _from, const Vec2& _to, const Colour& _colour = Colour::Black);

		static void RenderRect(const Rect& _rect, const Colour& _colour = Colour::Black);

		static void RenderRectFill(const Rect& _rect, const Colour& _colour = Colour::Black);

		static void RenderTexture(const Texture& _texture, const Colour& _colour);

		static void RenderTexture(const Texture& _texture, const Colour& _colour, const Transform& _transform);

	private:
		SDL_Renderer* m_renderer;

		Transform m_currentTransform;
		Transform m_parentTransform;

		std::stack<Transform> m_transforms;

	private:
		static Renderer* m_instance;
	};
}

#endif // GRAPEVINE_SDL_RENDERER