/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include <memory>

namespace Grapevine
{
	class AssetManager;
	class Window;

	// Application, controls the main game loop, initiating and destroying the SDL Window
	class Application
	{
		static Application* mInstance;

	public:
		static Application* GetInstance()
		{
			return mInstance;
		}

		void Run();

		Window* GetMainWindow() const { return mMainWindow.get(); }

		AssetManager& GetAssetManager() const { return *mAssetManager.get(); }

	protected:
		Application();

		virtual ~Application();

		virtual bool LoadMedia() { return true; }

		virtual void Start() {}

		virtual void Update() {}

		virtual void End() {}

		std::unique_ptr<AssetManager> mAssetManager;

	private:
		std::unique_ptr<Window> mMainWindow;
	};
}