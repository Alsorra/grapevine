/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

// this class
#include "Application.h"

// source includes
#include "../Application/AssetManager.h"
#include "../Application/Window.h"
#include "../Input/Input.h"
#include "../Input/Time.h"

namespace Grapevine
{
	Application* Application::mInstance = nullptr;

	Application::Application()
		: mMainWindow(new Window(1024, 768))
	{
		mInstance = this;
	}

	Application::~Application()
	{
		Time::DestroyInstance();
		Input::DestroyInstance();
	}

	void Application::Run()
	{
		if (!LoadMedia())
		{
			printf("Failed to load media!\n");
		}
		else
		{
			bool quit = false;

			Start();

			while (!quit)
			{
				Time::CalculateDeltaTime();

				Input::GetInstance()->UpdateInputEvents();

				Update();

				mMainWindow->UpdateAndRenderWindow();

				if (Input::Contains(InputKey::Quit))
				{
					quit = true;
				}
			}
		}

		End();
	}
}