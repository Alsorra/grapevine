/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

// this class
#include "Renderer.h"

// required includes
#include <SDL.h>
#include <SDL_image.h>

// source includes
#include "../Components/Physics/PhysicsWorldComponent.h"
#include "../Graphics/Texture.h"
#include "../Maths/Matrix3x3.h"
#include "../Objects/World.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

namespace Grapevine
{
	Renderer* Renderer::m_instance = nullptr;

	Renderer* Renderer::GetInstance()
	{
		if (m_instance == nullptr)
		{
			m_instance = new Renderer();
		}
		return m_instance;
	}

	void Renderer::DestroyInstance()
	{
		if (m_instance != nullptr)
		{
			delete m_instance;
			m_instance = nullptr;
		}
	}

	Renderer::Renderer()
		: m_renderer(nullptr)
	{
	}
	
	Renderer::~Renderer()
	{
		SDL_DestroyRenderer( m_renderer );
		m_renderer = nullptr;
	}

	void Renderer::RenderScreen(World& world)
	{
		//Clear screen
		SDL_SetRenderDrawColor( m_renderer, 0x00, 0x00, 0x00, 0xFF );
		SDL_RenderClear( m_renderer );

		world.Render();

		// render the physics
		//World::GetInstance()->GetPhysicsWorldComponent()->DebugRenderPhysics();

		//Update screen
		SDL_RenderPresent( m_renderer );
	}

	void Renderer::PushTransform(const Transform& _transform)
	{
		Transform newTransform;

		if (GetInstance()->m_transforms.empty())
		{
			newTransform = _transform;
		}
		else
		{
			newTransform = GetInstance()->m_transforms.top() + _transform;
		}

		GetInstance()->m_currentTransform = newTransform;
		GetInstance()->m_transforms.push(newTransform);
	}

	void Renderer::PopTransform()
	{
		if (GetInstance()->m_transforms.empty())
		{
			return;
		}

		//GetInstance()->m_currentTransform -= GetInstance()->m_transforms.top();
		//GetInstance()->m_transforms.pop();

		//if (!GetInstance()->m_transforms.empty())
		//{
		//	GetInstance()->m_parentTransform = GetInstance()->m_currentTransform - GetInstance()->m_transforms.top();
		//}
		//else
		//{
		//	GetInstance()->m_parentTransform.Clear();
		//}

		GetInstance()->m_transforms.pop();

		if (GetInstance()->m_transforms.empty())
		{
			GetInstance()->m_currentTransform.Clear();
		}
		else
		{
			GetInstance()->m_currentTransform = GetInstance()->m_transforms.top();
		}
	}

	void Renderer::ClearTransform()
	{
		GetInstance()->m_currentTransform.Clear();
		while (!GetInstance()->m_transforms.empty())
		{
			GetInstance()->m_transforms.pop();
		}
	}

	Vec2 Renderer::TransformPoint(const Vec2& _point)
	{
		return GetInstance()->m_currentTransform.GetTranslation() + _point;
	}

	Rect Renderer::TransformRect(const Rect& _rect)
	{
		Vec2 position = GetInstance()->m_currentTransform.GetTranslation();

		Rect rect(_rect);
		rect.left += (unsigned)position.x;
		rect.right += (unsigned)position.x;
		rect.top += (unsigned)position.y;
		rect.bottom += (unsigned)position.y;

		return rect;
	}

	void Renderer::RenderPixel(const Vec2& _pixel, const Colour& _colour)
	{
		Vec2 pixelPosition = TransformPoint(_pixel);

		SDL_SetRenderDrawColor(GetInstance()->m_renderer, _colour.r, _colour.g, _colour.b, _colour.a );
		SDL_RenderDrawPoint(GetInstance()->m_renderer, (int)pixelPosition.x, (int)pixelPosition.y );
	}

	void Renderer::RenderLine(const Vec2& _from, const Vec2& _to, const Colour& _colour)
	{
		Vec2 fromPosition = TransformPoint(_from);
		Vec2 toPosition = TransformPoint(_to);

		SDL_SetRenderDrawColor(GetInstance()->m_renderer, _colour.r, _colour.g, _colour.b, _colour.a );		
		SDL_RenderDrawLine(GetInstance()->m_renderer, (int)fromPosition.x, (int)fromPosition.y, (int)toPosition.x, (int)toPosition.y );
	}

	void Renderer::RenderRect(const Rect& _rect, const Colour& _colour)
	{
		Rect rect = TransformRect(_rect);

		SDL_Rect outlineRect = { (int)rect.left, (int)rect.top, (int)rect.GetWidth(), (int)rect.GetHeight() };
		SDL_SetRenderDrawColor(GetInstance()->m_renderer, _colour.r, _colour.g, _colour.b, _colour.a );		
		SDL_RenderDrawRect(GetInstance()->m_renderer, &outlineRect );
	}

	void Renderer::RenderRectFill(const Rect& _rect, const Colour& _colour)
	{
		Rect rect = TransformRect(_rect);

		SDL_Rect fillRect = { (int)rect.left, (int)rect.top, (int)rect.GetWidth(), (int)rect.GetHeight() };
		SDL_SetRenderDrawColor(GetInstance()->m_renderer, _colour.r, _colour.g, _colour.b, _colour.a );
		SDL_RenderFillRect(GetInstance()->m_renderer, &fillRect );
	}

	void Renderer::RenderTexture(const Texture& _texture, const Colour& _colour)
	{
		const Transform& world = GetInstance()->m_currentTransform;
		const Transform& local = GetInstance()->m_transforms.top();

		const Transform& parentWorld = GetInstance()->m_parentTransform;

		Transform transform;
		transform.SetTranslation(local.GetTranslation());
		transform.SetRotation(parentWorld.GetRotation());// + local.GetRotation());
		transform.SetScale(parentWorld.GetScale() * local.GetScale());

		Matrix3x3 parentMatrix = parentWorld.GetMatrix();
		Matrix3x3 localMatrix = transform.GetMatrix();

		Vec2 parentPos = parentWorld.GetTranslation();//parentMatrix.TransformPoint(parentWorld.GetTranslation());
		Vec2 localPos = transform.GetTranslation();// localMatrix.TransformPoint(transform.GetTranslation());

		Vec2 pos = parentPos + localPos;

		float scaledWidth = (float)_texture.GetWidth() * world.GetScale().x;
		float scaledHeight = (float)_texture.GetHeight() * world.GetScale().y;

		unsigned halfWidth = (unsigned)(scaledWidth / 2.0f);
		unsigned halfHeight = (unsigned)(scaledHeight / 2.0f);

		Rect destRect;

		destRect.left = (unsigned)pos.x - halfWidth;
		destRect.right = (unsigned)pos.x + halfWidth;
		destRect.top = (unsigned)pos.y - halfHeight;
		destRect.bottom = (unsigned)pos.y + halfHeight;

		// Set rendering space and render to screen
		SDL_Rect destQuad = { (int)destRect.left, (int)destRect.top, (int)destRect.GetWidth(), (int)destRect.GetHeight() };
		SDL_Rect srcQuad = { _texture.GetU(), _texture.GetV(), _texture.GetWidth(), _texture.GetHeight() };

		// Render to screen
		SDL_SetTextureColorMod(_texture.GetSDLTexture(), _colour.r, _colour.g, _colour.b);
		SDL_SetTextureAlphaMod(_texture.GetSDLTexture(), _colour.a);
		SDL_RenderCopyEx(GetInstance()->m_renderer, _texture.GetSDLTexture(), &srcQuad, &destQuad, world.GetRotation(), nullptr, SDL_FLIP_NONE);
	}

	void Renderer::RenderTexture(const Texture& _texture, const Colour& _colour, const Transform& _transform)
	{
		GetInstance()->PushTransform(_transform);

		RenderTexture(_texture, _colour);

		GetInstance()->PopTransform();
	}
}