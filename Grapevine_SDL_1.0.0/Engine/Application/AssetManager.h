/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_ASSETMANAGER
#define GRAPEVINE_SDL_ASSETMANAGER

// required includes
#include <map>

// source includes
#include "../../Engine/Graphics/Texture.h"
#include "../../Engine/Graphics/TrueTypeFont.h"

namespace Grapevine
{
	class Asset;

	// friend of Renderer and Texture
	class AssetManager
	{
		friend class Application;

		typedef std::map <std::string, unsigned>                 KeyMap;
		typedef std::pair<std::string, unsigned>                 KeyMapPair;
		typedef std::map <std::string, unsigned>::iterator       KeyMapIterator;
		typedef std::map <std::string, unsigned>::const_iterator KeyMapConstIterator;

		typedef std::map <unsigned, Asset*>                 AssetMap;
		typedef std::pair<unsigned, Asset*>                 AssetMapPair;
		typedef std::map <unsigned, Asset*>::iterator       AssetMapIterator;
		typedef std::map <unsigned, Asset*>::const_iterator AssetMapConstIterator;

	public:
		static void GenerateFontTexture(TextRenderer* _renderer);

		enum eAssetType
		{
			eAssetType_None,
			eAssetType_Texture,
			eAssetType_FontTexture
		};

		AssetManager(const std::string& _folderLocation);

		virtual ~AssetManager();

	public:
		bool LoadAssets();

	protected:
		virtual void SetupAssets() {}

		void SetupAsset(const std::string& _fileName, unsigned _key);

	private:
		bool LoadTexture(unsigned _id, const std::string& _path);

		bool LoadFontTexture(unsigned _id, const std::string& _path);

		eAssetType GetAssetType(const std::string& _file) const;

	public:
		bool GetAssetKey(const std::string& _file, unsigned& _key) const;

		template<class T> 
		T* GetAssetAs(unsigned _id) const;

	private:
		KeyMap m_keys;

		AssetMap m_assets;

		const std::string FolderLocation;
	};

	template<class T>
	T* AssetManager::GetAssetAs(unsigned _id) const
	{
		AssetMapConstIterator iter = m_assets.find(_id);

		if (iter != m_assets.end())
		{
			Asset* asset = iter->second;

			T* cast = dynamic_cast<T*>(asset);

			if (cast != nullptr)
			{
				return cast;
			}
		}

		return nullptr;
	}
}

#endif // GRAPEVINE_SDL_ASSETMANAGER