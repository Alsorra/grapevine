/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Component.h"

namespace Grapevine
{
	Component::Component(const std::string& _name)
		: Object(_name)
		, m_gameObject(NULL)
	{
	}

	Component::~Component()
	{
	}

	GameObject* Component::GetGameObject() const
	{ 
		return m_gameObject; 
	}

	Transform& Component::GetTransform()
	{
		return m_gameObject->GetTransform();
	}

	const Transform& Component::GetTransform() const
	{ 
		return m_gameObject->GetTransform(); 
	}
}