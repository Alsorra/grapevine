/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include "../Objects/GameObject.h"

namespace Grapevine
{
	class Transform;

	class Component : public Object
	{
		friend class GameObject;

	public:
		Component(const std::string& _name);

		virtual ~Component();

	public:
		GameObject* GetGameObject() const;

		Transform& GetTransform();
		const Transform& GetTransform() const;

	private:
		GameObject* m_gameObject;
	};
}