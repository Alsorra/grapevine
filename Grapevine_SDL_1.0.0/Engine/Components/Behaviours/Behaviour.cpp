/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Behaviour.h"

namespace Grapevine
{
	Behaviour::Behaviour(const std::string& _name)
		: Component(_name)
	{
	}
}