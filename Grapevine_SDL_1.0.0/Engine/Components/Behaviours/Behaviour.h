/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include "../Component.h"

namespace Grapevine
{
	class Behaviour : public Component
	{
	public:
		Behaviour(const std::string& _name);

	public:
		virtual void Start() = 0;
		virtual void Update() = 0;
		virtual void End() = 0;
	};
}