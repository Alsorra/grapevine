/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_PARTICLE
#define GRAPEVINE_SDL_PARTICLE

#include "../../../Maths/Transform.h"
#include "../../../Objects/Object.h"
#include "../../../Json/json.h"

namespace Grapevine
{
	struct ParticleSettings
	{
		unsigned m_activeCounter;

		unsigned _int8 m_alphaStartMin;
		unsigned _int8 m_alphaStartMax;

		unsigned _int8 m_alphaChangeSpeedMin;
		unsigned _int8 m_alphaChangeSpeedMax;

		Transform m_transformStartMin;
		Transform m_transformStartMax;

		Transform m_transformChangeMin;
		Transform m_transformChangeMax;

		void LoadFromJson(const Json::Value& _json);
	};

	class Particle : public Object
	{
	public:
		Particle(const ParticleSettings& _settings);

		virtual ~Particle();

		void UpdateParticle();

	private:
		Vec2 GetRandomVector2(const Vec2& _min, const Vec2& _max);

		template<class T>
		T GetRandomValue(const T& _min, const T& _max);

	public:
		float m_alpha;

		float m_alphaChangeSpeed;

		int m_activeCounter;

		bool m_active;

		Transform m_transform;

		Transform m_transformChangeUpdate;
	};

	template<class T>
	T Particle::GetRandomValue(const T& _min, const T& _max)
	{
		float t = (float)(rand() % 100) / 100.0f;

		return T(_min + ((_max - _min) * t));
	}
}

#endif