/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include "../../../Components/Behaviours/Behaviour.h"
#include "../../../Components/Behaviours/Particles/Particle.h"
#include "../../../Json/json.h"

#include <vector>
#include <memory>

namespace Grapevine
{
	class ParticleBehaviour : public Behaviour
	{
		friend class ParticleRenderer;

	public:
		ParticleBehaviour(const std::string& _settingsFile);
		virtual ~ParticleBehaviour();

		void Start() override {}
		void Update() override;
		void End() override {}

		void StartEmitting(bool _useCounter = false, int _counter = 0);
		void StopEmitting();

	private:
		void LoadParticleSettingsFromJson(const Json::Value& _json);

		void UpdateParticles();
		void UpdateSpawnParticles();
		void UpdateEmitCounter();

		int mMaxParticles;
		int mSpawnCounter;
		int mSpawnCounterMax;
		int mEmitCounter;

		bool mUseEmitCounter;
		bool mIsEmitting;

		std::vector<std::unique_ptr<Particle>> mParticles;

		ParticleSettings mSettings;
	};
}