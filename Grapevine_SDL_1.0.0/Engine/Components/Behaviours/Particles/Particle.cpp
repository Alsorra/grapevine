/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Particle.h"

#include "../../../../Engine/Input/Time.h"
#include "../../../../Engine/Json/JsonHelper.h"

namespace Grapevine
{
	void ParticleSettings::LoadFromJson(const Json::Value& _json)
	{
		m_activeCounter = _json["active_counter"].isNumeric() ? _json["active_counter"].asUInt() : 1000;

		m_alphaStartMin = _json["alpha_start_min"].isNumeric() ? _json["alpha_start_min"].asUInt() : 255;
		m_alphaStartMax = _json["alpha_start_max"].isNumeric() ? _json["alpha_start_max"].asUInt() : 255;

		m_alphaChangeSpeedMin = _json["alpha_change_speed_min"].isNumeric() ? _json["alpha_change_speed_min"].asUInt() : 0;
		m_alphaChangeSpeedMax = _json["alpha_change_speed_max"].isNumeric() ? _json["alpha_change_speed_max"].asUInt() : 0;
		
		m_transformStartMin = JsonHelper::LoadTransformFromJson(_json["transform_start_min"]);
		m_transformStartMax = JsonHelper::LoadTransformFromJson(_json["transform_start_max"]);

		m_transformChangeMin = JsonHelper::LoadTransformFromJson(_json["transform_change_min"]);
		m_transformChangeMax = JsonHelper::LoadTransformFromJson(_json["transform_change_max"]);
	}

	Particle::Particle(const ParticleSettings& _settings)
		: Object("particle")
		, m_alpha(0.0f)
		, m_alphaChangeSpeed(0.0f)
		, m_activeCounter(0)
		, m_active(true)
		, m_transform(Transform())
		, m_transformChangeUpdate(Transform())
	{
		m_alpha = GetRandomValue(_settings.m_alphaStartMin, _settings.m_alphaStartMax);

		m_alphaChangeSpeed = GetRandomValue(_settings.m_alphaChangeSpeedMin, _settings.m_alphaChangeSpeedMax);

		m_activeCounter = _settings.m_activeCounter;
				
		m_transform.SetTranslation(GetRandomVector2(_settings.m_transformStartMin.GetTranslation(), _settings.m_transformStartMax.GetTranslation()));
		m_transform.SetScale(GetRandomVector2(_settings.m_transformStartMin.GetScale(), _settings.m_transformStartMax.GetScale()));
		m_transform.SetRotation(GetRandomValue(_settings.m_transformStartMin.GetRotation(), _settings.m_transformStartMax.GetRotation()));

		m_transformChangeUpdate.SetTranslation(GetRandomVector2(_settings.m_transformChangeMin.GetTranslation(), _settings.m_transformChangeMax.GetTranslation()));
		m_transformChangeUpdate.SetScale(GetRandomVector2(_settings.m_transformChangeMin.GetScale(), _settings.m_transformChangeMax.GetScale()));
		m_transformChangeUpdate.SetRotation(GetRandomValue(_settings.m_transformChangeMin.GetRotation(), _settings.m_transformChangeMax.GetRotation()));
	}

	Particle::~Particle()
	{
	}

	void Particle::UpdateParticle()
	{
		if (m_active)
		{
			m_alpha += m_alphaChangeSpeed;

			m_transform.Translate(m_transformChangeUpdate.GetTranslation() * (float)Time::GetDeltaTime());
			m_transform.Rotate(m_transformChangeUpdate.GetRotation() * (float)Time::GetDeltaTime());
			m_transform.Scale(m_transformChangeUpdate.GetScale() * (float)Time::GetDeltaTime());

			m_activeCounter -= Time::GetDeltaTime();
			if (m_activeCounter <= 0)
			{
				m_active = false;
			}
		}
	}

	Vec2 Particle::GetRandomVector2(const Vec2& _min, const Vec2& _max)
	{
		Vec2 vector;
		vector.x = GetRandomValue(_min.x, _max.x);
		vector.y = GetRandomValue(_min.y, _max.y);

		return vector;
	}
}