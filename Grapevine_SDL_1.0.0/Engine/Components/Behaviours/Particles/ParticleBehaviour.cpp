/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

// this class
#include "ParticleBehaviour.h"

#include "../../../Input/Time.h"
#include "../../../Json/JsonHelper.h"

#include <algorithm>

namespace Grapevine
{
	ParticleBehaviour::ParticleBehaviour(const std::string& _settingsFile)
		: Behaviour("Particle Behaviour")
		, mMaxParticles(0)
		, mSpawnCounter(0)
		, mSpawnCounterMax(0)
		, mEmitCounter(0)
		, mUseEmitCounter(false)
		, mIsEmitting(false)
	{
		std::string path = "Assets\\Particles\\" + _settingsFile + ".json";

		Json::Value settings;
		if (JsonHelper::TryLoadJsonFromFile(path, settings))
		{
			LoadParticleSettingsFromJson(settings);
		}
	}

	ParticleBehaviour::~ParticleBehaviour()
	{
	}

	void ParticleBehaviour::LoadParticleSettingsFromJson(const Json::Value& _json)
	{
		mMaxParticles = _json["max_particles"].isNumeric() ? _json["max_particles"].asInt() : 10;
		mSpawnCounterMax = _json["spawn_counter"].isNumeric() ? _json["spawn_counter"].asInt() : 1000;

		mSettings.LoadFromJson(_json);
	}

	void ParticleBehaviour::Update()
	{
		UpdateParticles();

		if (mIsEmitting)
		{
			UpdateSpawnParticles();

			UpdateEmitCounter();
		}
	}

	void ParticleBehaviour::StartEmitting(bool _useCounter, int _counter)
	{
		mUseEmitCounter = _useCounter;
		mEmitCounter = _counter;

		mIsEmitting = true;
	}

	void ParticleBehaviour::StopEmitting()
	{
		mIsEmitting = false;
	}

	void ParticleBehaviour::UpdateParticles()
	{
		mParticles.erase(std::remove_if(mParticles.begin(), mParticles.end(), [](std::unique_ptr<Particle>& particle) {
				particle->UpdateParticle();

				return !particle->m_active;
			}), 
			mParticles.end());
	}

	void ParticleBehaviour::UpdateSpawnParticles()
	{
		mSpawnCounter += Time::GetDeltaTime();

		if (mSpawnCounter >= mSpawnCounterMax)
		{
			mSpawnCounter -= mSpawnCounterMax;

			if ((int)mParticles.size() < mMaxParticles)
			{
				mParticles.push_back(std::make_unique<Particle>(mSettings));
			}
		}
	}

	void ParticleBehaviour::UpdateEmitCounter()
	{
		if (mUseEmitCounter)
		{
			mEmitCounter -= Time::GetDeltaTime();
			if (mEmitCounter <= 0)
			{
				mIsEmitting = false;
			}
		}
	}
}