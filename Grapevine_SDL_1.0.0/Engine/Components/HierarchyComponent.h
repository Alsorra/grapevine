/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#pragma once

#include "../Components/Component.h"

#include <vector>

namespace Grapevine
{
	class HierarchyComponent : public Component
	{
		friend class GameObject;

	public:
		HierarchyComponent();
		virtual ~HierarchyComponent() = default;

		HierarchyComponent* GetParent() const { return m_parent; }
		HierarchyComponent* GetChild(const std::string& name) const;

		bool SetParent(HierarchyComponent* component);

	private:
		bool AddChild(HierarchyComponent* component);
		bool RemoveChild(HierarchyComponent* component);

		HierarchyComponent* m_parent;

		std::vector<HierarchyComponent*> m_children;
	};
}