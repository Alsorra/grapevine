/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "PhysicsBodyComponent.h"

#include "../../Application/Application.h"
#include "../../Application/Renderer.h"
#include "../../Application/Window.h"
#include "../../Components/Physics/PhysicsWorldComponent.h"
#include "../../Objects/World.h"

namespace Grapevine
{
	PhysicsBodyComponent::PhysicsBodyComponent(const Vec2& _bodySize)
		: Component("PhysicsBodyComponent")
		, m_bodyType(eBodyType_dynamic)
		, m_bodySize(_bodySize)
	{
		Application::GetInstance()->GetMainWindow()->GetMainObject().GetPhysicsWorldComponent()->AddBodyComponent(this);
	}

	PhysicsBodyComponent::PhysicsBodyComponent(const Vec2& _bodySize, eBodyType _type)
		: Component("PhysicsBodyComponent")
		, m_bodyType(_type)
		, m_bodySize(_bodySize)
	{
		Application::GetInstance()->GetMainWindow()->GetMainObject().GetPhysicsWorldComponent()->AddBodyComponent(this);
	}

	PhysicsBodyComponent::~PhysicsBodyComponent()
	{
		Application::GetInstance()->GetMainWindow()->GetMainObject().GetPhysicsWorldComponent()->RemoveBodyComponent(this);
	}

	void PhysicsBodyComponent::SetBodyType(eBodyType _type)
	{
		if (m_bodyType != _type)
		{
			m_onBodyTypeChanged.Emit(this, m_bodyType, _type);
			m_bodyType = _type;
		}
	}

	PhysicsBodyComponent::eBodyType PhysicsBodyComponent::GetBodyType() const
	{
		return m_bodyType;
	}

	Rect PhysicsBodyComponent::GetCollisionRectangle() const
	{
		Vec2 scale(GetTransform().GetScale());

		Vec2 size(m_bodySize.x * scale.x, m_bodySize.y * scale.y);
		Vec2 position(GetTransform().GetTranslation());

		Rect collision;
		collision.left = (unsigned)(position.x - (size.x / 2.0f));
		collision.right = (unsigned)(position.x + (size.x / 2.0f));
		collision.top = (unsigned)(position.y - (size.y / 2.0f));
		collision.bottom = (unsigned)(position.y + (size.y / 2.0f));

		return collision;
	}

	void PhysicsBodyComponent::DebugRenderBody() const
	{
		Rect collision = GetCollisionRectangle();

		Colour colour = Colour::White;

		switch (GetBodyType())
		{
		case eBodyType_dynamic: colour = Colour::Red; break;
		case eBodyType_kinematic: colour = Colour::Blue; break;
		case eBodyType_static: colour = Colour::Yellow; break;
		}

		Renderer::RenderRect(collision, colour);
	}

	PhysicsBodyComponent::OnComponentSignal& PhysicsBodyComponent::OnCollideWithSignal()
	{
		return m_onCollideWith;
	}

	PhysicsBodyComponent::OnBodyTypeChangedSignal& PhysicsBodyComponent::OnBodyTypeChanged()
	{
		return m_onBodyTypeChanged;
	}

	void PhysicsBodyComponent::PositionToTheLeftOf(PhysicsBodyComponent* _other)
	{
		Rect otherRect = _other->GetCollisionRectangle();
		Rect myRect = GetCollisionRectangle();

		Vec2 newPosition(GetTransform().GetTranslation());
		newPosition.x = otherRect.left - (myRect.GetWidth() / 2.0f);

		GetTransform().SetTranslation(newPosition);
	}

	void PhysicsBodyComponent::PositionToTheRightOf(PhysicsBodyComponent* _other)
	{
		Rect otherRect = _other->GetCollisionRectangle();
		Rect myRect = GetCollisionRectangle();

		Vec2 newPosition(GetTransform().GetTranslation());
		newPosition.x = otherRect.right + (myRect.GetWidth() / 2.0f);

		GetTransform().SetTranslation(newPosition);
	}

	void PhysicsBodyComponent::PositionAbove(PhysicsBodyComponent* _other)
	{
		Rect otherRect = _other->GetCollisionRectangle();
		Rect myRect = GetCollisionRectangle();

		Vec2 newPosition(GetTransform().GetTranslation());
		newPosition.y = otherRect.top - (myRect.GetHeight() / 2.0f);

		GetTransform().SetTranslation(newPosition);
	}

	void PhysicsBodyComponent::PositionBelow(PhysicsBodyComponent* _other)
	{
		Rect otherRect = _other->GetCollisionRectangle();
		Rect myRect = GetCollisionRectangle();

		Vec2 newPosition(GetTransform().GetTranslation());
		newPosition.y = otherRect.bottom + (myRect.GetHeight() / 2.0f);

		GetTransform().SetTranslation(newPosition);
	}

	void PhysicsBodyComponent::OnCollision(PhysicsBodyComponent* _other)
	{
		m_onCollideWith.Emit(_other);
	}
}