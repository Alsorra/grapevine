/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_PHYSICS_BODY_COMPONENT
#define GRAPEVINE_SDL_PHYSICS_BODY_COMPONENT

#include "../../Components/Component.h"
#include "../../Maths/Rect.h"
#include "../../Maths/Vector2.h"
#include "../../Signals/Signal.h"

namespace Grapevine
{
	class PhysicsBodyComponent : public Component
	{
		friend class PhysicsWorldComponent;
				
	public:
		enum eBodyType
		{
			eBodyType_dynamic,
			eBodyType_kinematic,
			eBodyType_static
		};

		typedef Gallant::Signal1<PhysicsBodyComponent*> OnComponentSignal;
			
		typedef Gallant::Signal3<PhysicsBodyComponent*, eBodyType, eBodyType> OnBodyTypeChangedSignal;
		
	public:
		PhysicsBodyComponent(const Vec2& _bodySize);

		PhysicsBodyComponent(const Vec2& _bodySize, eBodyType _type);

		virtual ~PhysicsBodyComponent();

		void SetBodyType(eBodyType _type);

		eBodyType GetBodyType() const;

		Rect GetCollisionRectangle() const;

		void DebugRenderBody() const;

		OnComponentSignal& OnCollideWithSignal();

		OnBodyTypeChangedSignal& OnBodyTypeChanged();

		void PositionToTheLeftOf(PhysicsBodyComponent* _other);

		void PositionToTheRightOf(PhysicsBodyComponent* _other);

		void PositionAbove(PhysicsBodyComponent* _other);

		void PositionBelow(PhysicsBodyComponent* _other);

	private:
		void OnCollision(PhysicsBodyComponent* _other);

	private:
		eBodyType m_bodyType;

		Vec2 m_bodySize;
		
		OnComponentSignal m_onCollideWith;

		OnBodyTypeChangedSignal m_onBodyTypeChanged;
	};
}

#endif // GRAPEVINE_SDL_PHYSICS_BODY_COMPONENT