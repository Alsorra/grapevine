/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_WORLD_PHYSICS_COMPONENT
#define GRAPEVINE_SDL_WORLD_PHYSICS_COMPONENT

#include "../../Components/Component.h"

#include "../../Components/Physics/PhysicsBodyComponent.h"

#include <map>
#include <vector>

namespace Grapevine
{
	class PhysicsWorldComponent : public Component
	{
		typedef std::vector<PhysicsBodyComponent*> BodyArray;
		typedef std::vector<PhysicsBodyComponent*>::iterator BodyArrayIterator;
		typedef std::vector<PhysicsBodyComponent*>::const_iterator BodyArrayConstIterator;

		typedef std::map<PhysicsBodyComponent::eBodyType, BodyArray> BodyArrayMap;

	public:
		PhysicsWorldComponent();

		void UpdatePhysics();

	private:
		void UpdateDynamicBodies();

		void UpdateKinematicBodies();

		void CheckForCollisionWithArray(PhysicsBodyComponent* _bodyA, PhysicsBodyComponent::eBodyType _type);

	public:
		void DebugRenderPhysics();

	private:
		void DebugRenderBodyArray(const BodyArray& _bodies) const;

	public:
		void AddBodyComponent(PhysicsBodyComponent* _body);

		void RemoveBodyComponent(PhysicsBodyComponent* _body);

	private:
		void AddBodyToList(PhysicsBodyComponent* _body, PhysicsBodyComponent::eBodyType _type);

		void RemoveBodyFromList(PhysicsBodyComponent* _body, PhysicsBodyComponent::eBodyType _type);

		void OnBodyTypeChanged(PhysicsBodyComponent* _body, PhysicsBodyComponent::eBodyType _from, PhysicsBodyComponent::eBodyType _to);

	private:
		BodyArrayMap m_bodies;
	};
}

#endif // GRAPEVINE_SDL_WORLD_PHYSICS_COMPONENT