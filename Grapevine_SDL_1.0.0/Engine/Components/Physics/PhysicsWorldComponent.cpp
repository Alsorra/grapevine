/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "PhysicsWorldComponent.h"

#include "../../Components/Physics/PhysicsBodyComponent.h"

namespace Grapevine
{
	PhysicsWorldComponent::PhysicsWorldComponent()
		: Component("PhysicsWorldComponent")
	{
		m_bodies.insert(std::make_pair<PhysicsBodyComponent::eBodyType, BodyArray>(PhysicsBodyComponent::eBodyType_dynamic, BodyArray()));
		m_bodies.insert(std::make_pair<PhysicsBodyComponent::eBodyType, BodyArray>(PhysicsBodyComponent::eBodyType_kinematic, BodyArray()));
		m_bodies.insert(std::make_pair<PhysicsBodyComponent::eBodyType, BodyArray>(PhysicsBodyComponent::eBodyType_static, BodyArray()));
	}

	void PhysicsWorldComponent::UpdatePhysics()
	{
		UpdateKinematicBodies();
		UpdateDynamicBodies();
	}

	void PhysicsWorldComponent::UpdateDynamicBodies()
	{
		BodyArray& dynamicBodies = m_bodies[PhysicsBodyComponent::eBodyType_dynamic];

		for (BodyArrayIterator dynamicIter = dynamicBodies.begin(); dynamicIter != dynamicBodies.end(); ++dynamicIter)
		{
			PhysicsBodyComponent* bodyA = (*dynamicIter);
			if (!bodyA->m_enabled)
			{
				continue;
			}

			CheckForCollisionWithArray(bodyA, PhysicsBodyComponent::eBodyType_dynamic);
			CheckForCollisionWithArray(bodyA, PhysicsBodyComponent::eBodyType_kinematic);
			CheckForCollisionWithArray(bodyA, PhysicsBodyComponent::eBodyType_static);
		}
	}

	void PhysicsWorldComponent::UpdateKinematicBodies()
	{

	}

	void PhysicsWorldComponent::CheckForCollisionWithArray(PhysicsBodyComponent* _bodyA, PhysicsBodyComponent::eBodyType _type)
	{
		BodyArray& bodies = m_bodies[_type];

		BodyArrayIterator iter = bodies.begin();

		// only test with bodies after itself within the array to avoid
		// checking the same bodies together multiple times in a frame
		if (_bodyA->GetBodyType() == _type)
		{
			iter = std::find(bodies.begin(), bodies.end(), _bodyA);
			if (iter == bodies.end())
			{
				return;
			}
		}

		for (; iter != bodies.end(); ++iter)
		{
			PhysicsBodyComponent* bodyB = (*iter);
			if (bodyB->m_enabled)
			{
				Rect collisionA = _bodyA->GetCollisionRectangle();
				Rect collisionB = bodyB->GetCollisionRectangle();

				if (collisionA.IntersectionOccurs(collisionB))
				{
					_bodyA->OnCollision(bodyB);
					bodyB->OnCollision(_bodyA);
				}
			}
		}
	}

	void PhysicsWorldComponent::DebugRenderPhysics()
	{
		DebugRenderBodyArray(m_bodies[PhysicsBodyComponent::eBodyType_dynamic]);
		DebugRenderBodyArray(m_bodies[PhysicsBodyComponent::eBodyType_kinematic]);
		DebugRenderBodyArray(m_bodies[PhysicsBodyComponent::eBodyType_static]);
	}

	void PhysicsWorldComponent::DebugRenderBodyArray(const BodyArray& _bodies) const
	{
		for (BodyArrayConstIterator iter = _bodies.begin(); iter != _bodies.end(); ++iter)
		{
			PhysicsBodyComponent* body = (*iter);
			if (body->m_enabled)
			{
				body->DebugRenderBody();
			}
		}
	}

	void PhysicsWorldComponent::AddBodyComponent(PhysicsBodyComponent* _body)
	{
		AddBodyToList(_body, _body->GetBodyType());

		_body->OnBodyTypeChanged().Connect(this, &PhysicsWorldComponent::OnBodyTypeChanged);
	}

	void PhysicsWorldComponent::RemoveBodyComponent(PhysicsBodyComponent* _body)
	{
		_body->OnBodyTypeChanged().Disconnect(this, &PhysicsWorldComponent::OnBodyTypeChanged);

		RemoveBodyFromList(_body, _body->GetBodyType());
	}

	void PhysicsWorldComponent::AddBodyToList(PhysicsBodyComponent* _body, PhysicsBodyComponent::eBodyType _type)
	{
		BodyArray& bodies = m_bodies[_type];

		if (std::find(bodies.begin(), bodies.end(), _body) == bodies.end())
		{
			bodies.push_back(_body);
		}
	}

	void PhysicsWorldComponent::RemoveBodyFromList(PhysicsBodyComponent* _body, PhysicsBodyComponent::eBodyType _type)
	{
		BodyArray& bodies = m_bodies[_type];

		BodyArrayIterator iter = std::find(bodies.begin(), bodies.end(), _body);

		if (iter != bodies.end())
		{
			bodies.erase(iter);
		}
	}

	void PhysicsWorldComponent::OnBodyTypeChanged(PhysicsBodyComponent* _body, PhysicsBodyComponent::eBodyType _from, PhysicsBodyComponent::eBodyType _to)
	{
		RemoveBodyFromList(_body, _from);
		AddBodyToList(_body, _to);
	}
}