/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "HierarchyComponent.h"

namespace Grapevine
{
	HierarchyComponent::HierarchyComponent()
		: Component("Hierarchy")
		, m_parent(nullptr)
	{
	}

	HierarchyComponent* HierarchyComponent::GetChild(const std::string& name) const
	{
		for (auto& child : m_children)
		{
			if (child->GetGameObject() && child->GetGameObject()->m_name == name)
			{
				return child;
			}
		}

		return nullptr;
	}

	bool HierarchyComponent::SetParent(HierarchyComponent* parent)
	{
		if (m_parent == parent)
		{
			return false;
		}

		if (m_parent != nullptr)
		{
			m_parent->RemoveChild(this);
			m_parent = nullptr;
		}

		if (parent != nullptr)
		{
			parent->AddChild(this);
		}

		m_parent = parent;

		return true;
	}

	bool HierarchyComponent::AddChild(HierarchyComponent* component)
	{
		if (component->GetParent() != nullptr)
		{
			return false;
		}

		m_children.push_back(component);

		return true;
	}

	bool HierarchyComponent::RemoveChild(HierarchyComponent* component)
	{
		if (component->GetParent() != this)
		{
			return false;
		}

		auto& iter = std::find(m_children.begin(), m_children.end(), component);
		if (iter != m_children.end())
		{
			m_children.erase(iter);
			return true;
		}

		return false;
	}
}