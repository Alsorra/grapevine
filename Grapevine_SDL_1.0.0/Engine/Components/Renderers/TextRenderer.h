/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_TEXTRENDERER
#define GRAPEVINE_SDL_TEXTRENDERER

// source includes
#include "../../../Engine/Components/Renderers/RendererComponent.h"
#include "../../../Engine/Graphics/Colour.h"
#include "../../../Engine/Graphics/Texture.h"

namespace Grapevine
{
	class TrueTypeFont;

	class TextRenderer : public RendererComponent
	{
		friend class AssetManager;

	public:
		TextRenderer();

		TextRenderer(TrueTypeFont* _font, const std::string& _text);

		void GenerateTexture();

	public:
		virtual void Render() override;

		void SetFont(TrueTypeFont* _font);
		
		void SetColour(const Colour& _colour);

		void SetText(const std::string& _text);

		const Colour& GetColour() const;

		const std::string& GetText() const;
		
	protected:
		TrueTypeFont* m_font;

		Texture m_texture;

		Colour m_colour;

		std::string m_text;
	};
}

#endif // GRAPEVINE_SDL_TEXTRENDERER