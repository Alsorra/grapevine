/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_RENDERERCOMPONENT
#define GRAPEVINE_SDL_RENDERERCOMPONENT

#include "..\Component.h"

namespace Grapevine
{
	class RendererComponent : public Component
	{
	public:
		RendererComponent(const std::string& _name);

	public:
		virtual void Render() = 0;
	};
}

#endif // GRAPEVINE_SDL_RENDERERCOMPONENT