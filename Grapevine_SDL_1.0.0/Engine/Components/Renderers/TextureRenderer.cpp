/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

// this class
#include "TextureRenderer.h"

// source includes
#include "../../Application/Application.h"
#include "../../Application/AssetManager.h"
#include "../../Application/Renderer.h"
#include "../../Graphics/Texture.h"
#include "../../Maths/Rect.h"

namespace Grapevine
{
	TextureRenderer::TextureRenderer()
		: RendererComponent("Texture Renderer")
		, m_texture(nullptr)
		, m_colour(Colour::White)
	{}

	TextureRenderer::TextureRenderer(const std::string& _name)
		: RendererComponent(_name)
		, m_texture(nullptr)
		, m_colour(Colour::White)
	{}

	TextureRenderer::TextureRenderer(Texture* _texture)
		: RendererComponent("Texture Renderer")
		, m_texture(_texture)
		, m_colour(Colour::White)
	{}

	void TextureRenderer::Render()
	{
		if (m_texture == nullptr)
		{
			return;
		}

		Renderer::RenderTexture(*m_texture, m_colour);
	}

	void TextureRenderer::SetTexture(Texture* _texture)
	{
		m_texture = _texture;
	}

	void TextureRenderer::SetColour(const Colour& _colour)
	{
		m_colour = _colour;
	}

	const Colour& TextureRenderer::GetColour() const
	{
		return m_colour;
	}
}