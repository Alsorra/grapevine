/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

// this class
#include "TextRenderer.h"

// source includes
#include "../../../Engine/Application/AssetManager.h"
#include "../../../Engine/Application/Renderer.h"
#include "../../../Engine/Graphics/Texture.h"
#include "../../../Engine/Graphics/TrueTypeFont.h"

//
#include <SDL_ttf.h>

namespace Grapevine
{
	TextRenderer::TextRenderer()
		: RendererComponent("Text Renderer")
		, m_font(nullptr)
		, m_texture()
		, m_colour(Colour::Black)
		, m_text("")
	{}

	TextRenderer::TextRenderer(TrueTypeFont* _font, const std::string& _text)
		: RendererComponent("Text Renderer")
		, m_font(_font)
		, m_texture()
		, m_colour(Colour::Black)
		, m_text(_text)
	{
		GenerateTexture();
	}
	
	void TextRenderer::GenerateTexture()
	{
		AssetManager::GenerateFontTexture(this);
	}

	void TextRenderer::Render()
	{
		if (m_font == nullptr
			|| m_texture.GetSDLTexture() == nullptr)
		{
			return;
		}

		Renderer::RenderTexture(m_texture, m_colour);
	}

	void TextRenderer::SetFont(TrueTypeFont* _font)
	{
		if (m_font != _font)
		{
			m_font = _font;
			GenerateTexture();
		}
	}

	void TextRenderer::SetColour(const Colour& _colour)
	{
		if (m_colour != _colour)
		{
			m_colour = _colour;
			GenerateTexture();
		}
	}

	void TextRenderer::SetText(const std::string& _text)
	{
		if (m_text != _text)
		{
			m_text = _text;
			GenerateTexture();
		}
	}

	const Colour& TextRenderer::GetColour() const
	{
		return m_colour;
	}

	const std::string& TextRenderer::GetText() const
	{
		return m_text;
	}
}