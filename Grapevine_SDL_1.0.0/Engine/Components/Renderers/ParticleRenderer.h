/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_PARTICLERENDERER
#define GRAPEVINE_SDL_PARTICLERENDERER

// source includes
#include "../../Components/Renderers/TextureRenderer.h"
#include "../../Graphics/Colour.h"

namespace Grapevine
{
	class ParticleBehaviour;
	class Texture;

	class ParticleRenderer : public TextureRenderer
	{
	public:
		ParticleRenderer(ParticleBehaviour* _behaviour);

		~ParticleRenderer();

	public:
		void Render() override;

	private:
		ParticleBehaviour* m_behaviour;
	};
}

#endif