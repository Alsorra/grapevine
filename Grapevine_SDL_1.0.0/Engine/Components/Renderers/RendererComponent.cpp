/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "RendererComponent.h"

namespace Grapevine
{
	RendererComponent::RendererComponent(const std::string& _name)
		: Component(_name)
	{
	}
}