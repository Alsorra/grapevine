/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_TEXTURERENDERER
#define GRAPEVINE_SDL_TEXTURERENDERER

// source includes
#include "RendererComponent.h"
#include "../../Graphics/Colour.h"

namespace Grapevine
{
	class Texture;

	class TextureRenderer : public RendererComponent
	{
	public:
		TextureRenderer();

		TextureRenderer(const std::string& _name);

		TextureRenderer(Texture* _texture);

	public:
		virtual void Render() override;

		void SetTexture(Texture* _texture);

		void SetColour(const Colour& _colour);

		const Colour& GetColour() const;

	protected:
		Texture* m_texture;

		Colour m_colour;
	};
}

#endif // GRAPEVINE_SDL_TEXTURERENDERER