/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

// this class
#include "ParticleRenderer.h"

// source includes
#include "../../Application/Application.h"
#include "../../Application/AssetManager.h"
#include "../../Application/Renderer.h"
#include "../../Components/Behaviours/Particles/ParticleBehaviour.h"
#include "../../Graphics/Texture.h"
#include "../../Maths/Rect.h"

namespace Grapevine
{
	ParticleRenderer::ParticleRenderer(ParticleBehaviour* _behaviour)
		: TextureRenderer("Partical Renderer")
		, m_behaviour(_behaviour)
	{
	}

	ParticleRenderer::~ParticleRenderer()
	{
	}

	void ParticleRenderer::Render()
	{
		if (m_texture == nullptr)
		{
			return;
		}

		for (auto& particle : m_behaviour->mParticles)
		{
			Colour colour(m_colour);
			colour.a = (unsigned _int8)(255.0f * particle->m_alpha);

			Renderer::RenderTexture(*m_texture, colour, particle->m_transform);
		}
	}
}