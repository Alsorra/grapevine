/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Texture.h"

#include <SDL.h>

namespace Grapevine
{
	Texture::Texture()
		: Asset()
		, m_sdlTexture(nullptr)
		, m_u(0)
		, m_v(0)
		, m_width(0)
		, m_height(0)
	{}

	Texture::~Texture()
	{
		// Free Texture
		if (m_sdlTexture)
		{
			SDL_DestroyTexture(m_sdlTexture);
			m_sdlTexture = nullptr;
		}
	}

	void Texture::Reset()
	{
		Asset::Reset();

		// Free Texture
		if (m_sdlTexture)
		{
			SDL_DestroyTexture(m_sdlTexture);
			m_sdlTexture = nullptr;
		}

		m_u = 0;
		m_v = 0;
		m_width = 0;
		m_height = 0;
	}

	SDL_Texture* Texture::GetSDLTexture() const 
	{ 
		return m_sdlTexture; 
	}

	unsigned _int16 Texture::GetU() const 
	{ 
		return m_u; 
	}

	unsigned _int16 Texture::GetV() const 
	{ 
		return m_v; 
	}

	unsigned _int16 Texture::GetWidth() const 
	{ 
		return m_width; 
	}

	unsigned _int16 Texture::GetHeight() const 
	{ 
		return m_height; 
	}
}