/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_COLOUR
#define GRAPEVINE_SDL_COLOUR

namespace Grapevine
{
	struct Colour
	{
	public:
		Colour();

		Colour(unsigned _int8 _r, unsigned _int8 _g, unsigned _int8 _b);

		Colour(unsigned _int8 _r, unsigned _int8 _g, unsigned _int8 _b, unsigned _int8 _a);

		void Set(unsigned _int8 _r, unsigned _int8 _g, unsigned _int8 _b);

		void Set(unsigned _int8 _r, unsigned _int8 _g, unsigned _int8 _b, unsigned _int8 _a);

		bool operator==(const Colour& _other) const;

		bool operator!=(const Colour& _other) const;

	public:
		unsigned _int8 r;
		unsigned _int8 g;
		unsigned _int8 b;
		unsigned _int8 a;

	public:
		const static Colour Black;
		const static Colour White;
		const static Colour Red;
		const static Colour Green;
		const static Colour Blue;
		const static Colour Yellow;
	};
}

#endif // GRAPEVINE_SDL_COLOUR