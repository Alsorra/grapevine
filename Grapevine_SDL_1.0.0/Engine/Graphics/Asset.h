/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_ASSET
#define GRAPEVINE_SDL_ASSET

namespace Grapevine
{
	class Asset
	{
	private:
		friend class AssetManager;

	protected:
		Asset();

		virtual ~Asset();

		virtual void Reset();

	public:
		unsigned GetAssetId() const;

	private:
		unsigned m_assetId;
	};
}

#endif // GRAPEVINE_SDL_ASSET