/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Colour.h"

namespace Grapevine
{
	const Colour Colour::Black = Colour(0, 0, 0, 255);
	const Colour Colour::White = Colour(255, 255, 255, 255);
	const Colour Colour::Red = Colour(255, 0, 0, 255);
	const Colour Colour::Green = Colour(0, 255, 0, 255);
	const Colour Colour::Blue = Colour(0, 0, 255, 255);
	const Colour Colour::Yellow = Colour(255, 255, 0, 255);

	Colour::Colour()
		: r(0)
		, g(0)
		, b(0)
		, a(0)
	{}

	Colour::Colour(unsigned _int8 _r, unsigned _int8 _g, unsigned _int8 _b)
		: r(_r)
		, g(_g)
		, b(_b)
		, a(255)
	{}

	Colour::Colour(unsigned _int8 _r, unsigned _int8 _g, unsigned _int8 _b, unsigned _int8 _a)
		: r(_r)
		, g(_g)
		, b(_b)
		, a(_a)
	{}

	void Colour::Set(unsigned _int8 _r, unsigned _int8 _g, unsigned _int8 _b)
	{
		Set(_r, _g, _b, 255);
	}

	void Colour::Set(unsigned _int8 _r, unsigned _int8 _g, unsigned _int8 _b, unsigned _int8 _a)
	{
		r = _r;
		g = _g;
		b = _b;
		a = _a;
	}

	bool Colour::operator==(const Colour& _other) const
	{
		return r == _other.r
			&& g == _other.g
			&& b == _other.b
			&& a == _other.a;
	}

	bool Colour::operator!=(const Colour& _other) const
	{
		return !(*this == _other);
	}
}