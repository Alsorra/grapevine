/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_TRUETYPEFONT
#define GRAPEVINE_SDL_TRUETYPEFONT

#include "../../Engine/Graphics/Asset.h"

typedef struct _TTF_Font TTF_Font;

namespace Grapevine
{
	class TrueTypeFont : public Asset
	{
	private:
		friend class AssetManager;

	private:
		TrueTypeFont();

		virtual ~TrueTypeFont();

	public:
		TTF_Font* GetTTFFont() const;

	private:
		TTF_Font* m_ttfFont;
	};
}

#endif // GRAPEVINE_SDL_TRUETYPEFONT