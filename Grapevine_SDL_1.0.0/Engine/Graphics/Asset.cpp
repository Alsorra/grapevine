/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "Asset.h"

namespace Grapevine
{
	Asset::Asset()
		: m_assetId(0)
	{}

	Asset::~Asset()
	{}

	unsigned Asset::GetAssetId() const
	{
		return m_assetId;
	}

	void Asset::Reset()
	{
		m_assetId = 0;
	}
}