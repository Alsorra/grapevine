/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#ifndef GRAPEVINE_SDL_TEXTURE
#define GRAPEVINE_SDL_TEXTURE

#include "../../Engine/Graphics/Asset.h"

struct SDL_Texture;

namespace Grapevine
{
	class Texture : public Asset
	{
	private:
		// these are the classes which are allowed to create textures
		friend class AssetManager;
		friend class TextRenderer;

	private:
		Texture();

		virtual ~Texture();

		virtual void Reset() override;

	public:
		SDL_Texture* GetSDLTexture() const;

		unsigned _int16 GetU() const;

		unsigned _int16 GetV() const;

		unsigned _int16 GetWidth() const;

		unsigned _int16 GetHeight() const;

	private:
		SDL_Texture* m_sdlTexture;
		
		unsigned _int16 m_u;
		unsigned _int16 m_v;
		unsigned _int16 m_width;
		unsigned _int16 m_height;
	};
}

#endif // GRAPEVINE_SDL_TEXTURE