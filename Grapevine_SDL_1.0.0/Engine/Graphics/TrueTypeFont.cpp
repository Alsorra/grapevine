/*
 * 
 * Grapevine SDL 1.0.0
 *
 * Games Engine
 * 
 * Written and Owned by Alice Thorntonsmith
 * 
 */

#include "TrueTypeFont.h"

#include <SDL_ttf.h>

namespace Grapevine
{
	TrueTypeFont::TrueTypeFont()
		: Asset()
		, m_ttfFont(NULL)
	{
	}

	TrueTypeFont::~TrueTypeFont()
	{
		// Free Texture
		if (m_ttfFont)
		{
			TTF_CloseFont(m_ttfFont);
			m_ttfFont = NULL;
		}
	}

	TTF_Font* TrueTypeFont::GetTTFFont() const 
	{ 
		return m_ttfFont; 
	}
}