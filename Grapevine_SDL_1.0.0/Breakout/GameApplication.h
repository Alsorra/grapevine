/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

#pragma once

// engine includes
#include "../Engine/Application/Application.h"
#include "../Engine/Objects/GameObject.h"

namespace Breakout
{
	class BrickBehaviour;

	class GameApplication : public Grapevine::Application
	{
	public:
		static std::string BatTag;
		static std::string BallTag;
		static std::string BrickTag;
		static std::string LeftWallTag;
		static std::string RightWallTag;
		static std::string CeilingTag;
		static std::string FloorTag;

	public:
		//! constructor
		GameApplication();

		//! destructor
		~GameApplication();

	private:
		bool LoadMedia() override;

		void Start() override;

		void Update() override;

		void End() override;

		void OnBrickDestroyed(BrickBehaviour* _behaviour);

		void LoadLevel(const std::string& _path);

	private:
		Grapevine::GameObject* m_bat;
		Grapevine::GameObject* m_ball;

		std::vector<BrickBehaviour*> m_destroyedBricks;
	};
}