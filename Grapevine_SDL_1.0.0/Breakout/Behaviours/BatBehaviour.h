/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

#pragma once

#include "../../Engine/Components/Behaviours/Behaviour.h"
#include "../../Engine/Maths/Rect.h"
#include "../../Engine/Objects/GameObject.h"

namespace Grapevine
{
	class PhysicsBodyComponent;
}

namespace Breakout
{
	class BatBehaviour : public Grapevine::Behaviour
	{
	public:
		BatBehaviour();

		virtual ~BatBehaviour();

	public:
		void Start() override;

		void Update() override;

		void End() override;

	private:
		void OnCollideWith(Grapevine::PhysicsBodyComponent* _other);

	private:
		Grapevine::PhysicsBodyComponent* m_bodyComponent;
	};
}