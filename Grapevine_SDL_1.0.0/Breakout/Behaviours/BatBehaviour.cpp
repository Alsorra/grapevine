/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

// this class
#include "BatBehaviour.h"

// source includes
#include "../AssetIDs.h"
#include "../GameApplication.h"
#include "../GameAssetManager.h"

// engine includes
#include "../../Engine/Components/Physics/PhysicsBodyComponent.h"
#include "../../Engine/Components/Renderers/TextureRenderer.h"
#include "../../Engine/Graphics/Colour.h"
#include "../../Engine/Graphics/Texture.h"
#include "../../Engine/Input/Input.h"
#include "../../Engine/Input/Time.h"
#include "../../Engine/Maths/Maths.h"
#include "../../Engine/Objects/GameObject.h"

namespace Breakout
{
	BatBehaviour::BatBehaviour()
		: Behaviour("Bat Behaviour")
		, m_bodyComponent(NULL)
	{
	}

	BatBehaviour::~BatBehaviour()
	{
	}

	void BatBehaviour::Start()
	{
		GetTransform().Translate(Vec2(512.0f, 700.0f));
		GetTransform().SetScale(Vec2(1.0f, 1.0f));

		TextureRenderer* renderer = new TextureRenderer();
		GetGameObject()->AddComponent(renderer);

		Texture* texture = GameApplication::GetInstance()->GetAssetManager().GetAssetAs<Texture>(AssetIDs::Bat);

		renderer->SetTexture(texture);
		renderer->SetColour(Colour::Red);

		m_bodyComponent = new PhysicsBodyComponent(Vec2(texture->GetWidth(), texture->GetHeight()));
		m_bodyComponent->OnCollideWithSignal().Connect(this, &BatBehaviour::OnCollideWith);
		GetGameObject()->AddComponent(m_bodyComponent);
	}

	void BatBehaviour::Update()
	{
		float xMovement = (float)Time::GetDeltaTime() * 1.0f;

		if (Input::Contains(InputKey::Left))
		{
			this->GetTransform().Translate(Vec2(-xMovement, 0.0f));
		}
		else if (Input::Contains(InputKey::Right))
		{
			this->GetTransform().Translate(Vec2(xMovement, 0.0f));
		}
	}

	void BatBehaviour::End()
	{
	}

	void BatBehaviour::OnCollideWith(PhysicsBodyComponent* _other)
	{
		const std::string& objectName = _other->GetGameObject()->m_name;

		if (objectName == GameApplication::LeftWallTag)
		{
			m_bodyComponent->PositionToTheRightOf(_other);
		}
		else if (objectName == GameApplication::RightWallTag)
		{
			m_bodyComponent->PositionToTheLeftOf(_other);
		}
	}
}