/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

#pragma once

#include "../../Engine/Components/Behaviours/Behaviour.h"
#include "../../Engine/Maths/Rect.h"
#include "../../Engine/Objects/GameObject.h"
#include "../../Engine/Signals/Signal.h"

namespace Grapevine
{
	class ParticleObject;
	class PhysicsBodyComponent;
	class TextureRenderer;
}

namespace Breakout
{
	class BrickBehaviour : public Grapevine::Behaviour
	{
		typedef Gallant::Signal1<BrickBehaviour*> OnBrickSignal;

		typedef std::vector<Grapevine::ParticleObject*> BrickParticleArray;

	public:
		BrickBehaviour(int _health);

		virtual ~BrickBehaviour();

	public:
		void Start() override;

		void Update() override;

		void End() override;

		OnBrickSignal& OnBrickDestroyedSignal();

	private:
		void OnCollideWith(Grapevine::PhysicsBodyComponent* _other);

		void UpdateColour();

	private:
		Grapevine::PhysicsBodyComponent* m_bodyComponent;

		Grapevine::TextureRenderer* m_renderer;

		int m_brickHealth;

		OnBrickSignal m_onBrickDestroyed;

		BrickParticleArray m_particles;
	};
}