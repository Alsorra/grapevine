/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

#pragma once

#include "../../Engine/Components/Behaviours/Behaviour.h"
#include "../../Engine/Maths/Rect.h"
#include "../../Engine/Objects/GameObject.h"

namespace Grapevine
{
	class PhysicsBodyComponent;
}

namespace Breakout
{
	class BallBehaviour : public Grapevine::Behaviour
	{
	public:
		BallBehaviour();

		virtual ~BallBehaviour();

	public:
		void Start() override;

		void Update() override;

		void End() override;

	private:
		void OnCollideWith(Grapevine::PhysicsBodyComponent* _other);

	private:
		Grapevine::PhysicsBodyComponent* m_bodyComponent;

		Vec2 m_velocity;
	};
}