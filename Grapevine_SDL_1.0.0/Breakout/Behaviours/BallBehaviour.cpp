/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

// this class
#include "BallBehaviour.h"

// source includes
#include "../AssetIDs.h"
#include "../GameApplication.h"
#include "../GameAssetManager.h"

// engine includes
#include "../../Engine/Components/Physics/PhysicsBodyComponent.h"
#include "../../Engine/Components/Renderers/TextureRenderer.h"
#include "../../Engine/Graphics/Colour.h"
#include "../../Engine/Graphics/Texture.h"
#include "../../Engine/Input/Input.h"
#include "../../Engine/Input/Time.h"
#include "../../Engine/Maths/Maths.h"
#include "../../Engine/Objects/GameObject.h"

namespace Breakout
{
	BallBehaviour::BallBehaviour()
		: Behaviour("Ball Behaviour")
		, m_bodyComponent()
		, m_velocity(Vec2(-0.2f, -0.2f))
	{
	}

	BallBehaviour::~BallBehaviour()
	{
	}

	void BallBehaviour::Start()
	{
		GetTransform().Translate(Vec2(512.0f, 700.0f));
		GetTransform().SetScale(Vec2(1.0f, 1.0f));

		TextureRenderer* renderer = new TextureRenderer();
		GetGameObject()->AddComponent(renderer);

		Texture* texture = GameApplication::GetInstance()->GetAssetManager().GetAssetAs<Texture>(AssetIDs::Ball);

		renderer->SetTexture(texture);
		renderer->SetColour(Colour::Blue);

		m_bodyComponent = new PhysicsBodyComponent(Vec2(texture->GetWidth(), texture->GetHeight()));
		m_bodyComponent->OnCollideWithSignal().Connect(this, &BallBehaviour::OnCollideWith);
		GetGameObject()->AddComponent(m_bodyComponent);
	}

	void BallBehaviour::Update()
	{
		Vec2 movement(m_velocity.x * Time::GetDeltaTime(), m_velocity.y * Time::GetDeltaTime());

		this->GetTransform().Translate(movement);
	}

	void BallBehaviour::End()
	{
	}

	void BallBehaviour::OnCollideWith(PhysicsBodyComponent* _other)
	{
		const std::string& objectName = _other->GetGameObject()->m_name;

		if (objectName == GameApplication::BatTag)
		{
			//if (GetTransform()->GetTranslation().x > _other->GetCollisionRectangle().top)
			{
				m_bodyComponent->PositionAbove(_other);
				m_velocity.y *= -1.0f;
			}
		}
		else if (objectName == GameApplication::LeftWallTag)
		{
			m_bodyComponent->PositionToTheRightOf(_other);
			m_velocity.x *= -1.0f;
		}
		else if (objectName == GameApplication::RightWallTag)
		{
			m_bodyComponent->PositionToTheLeftOf(_other);
			m_velocity.x *= -1.0f;
		}
		else if (objectName == GameApplication::CeilingTag)
		{
			m_bodyComponent->PositionBelow(_other);
			m_velocity.y *= -1.0f;
		}
		else if(objectName == GameApplication::FloorTag)
		{
			m_bodyComponent->PositionAbove(_other);
			m_velocity.y *= -1.0f;
		}
		else if (objectName == GameApplication::BrickTag)
		{
			const Vec2& thisTrans = GetTransform().GetTranslation();
			const Vec2& othrTrans = _other->GetTransform().GetTranslation();

			bool onLeft = thisTrans.x < othrTrans.x;
			bool above = thisTrans.y < othrTrans.y;

			Vec2 diff = thisTrans - othrTrans;

			if (Maths::Abs(diff.x) > Maths::Abs(diff.y))
			{
				if (onLeft)
				{
					m_bodyComponent->PositionToTheLeftOf(_other);
				}
				else
				{
					m_bodyComponent->PositionToTheRightOf(_other);
				}
				m_velocity.x *= -1.0f;
			}
			else
			{
				if (above)
				{
					m_bodyComponent->PositionAbove(_other);
				}
				else
				{
					m_bodyComponent->PositionBelow(_other);
				}
				m_velocity.y *= -1.0f;
			}
		}
	}
}