/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

// this class
#include "BrickBehaviour.h"

// source includes
#include "../AssetIDs.h"
#include "../GameApplication.h"
#include "../GameAssetManager.h"

// engine includes
#include "../../Engine/Components/Physics/PhysicsBodyComponent.h"
#include "../../Engine/Components/Renderers/TextureRenderer.h"
#include "../../Engine/Graphics/Colour.h"
#include "../../Engine/Graphics/Texture.h"
#include "../../Engine/Input/Input.h"
#include "../../Engine/Input/Time.h"
#include "../../Engine/Maths/Maths.h"
#include "../../Engine/Objects/GameObject.h"
#include "../../Engine/Objects/ParticleObject.h"

namespace Breakout
{
	BrickBehaviour::BrickBehaviour(int _health)
		: Behaviour("Brick Behaviour")
		, m_bodyComponent(nullptr)
		, m_renderer(nullptr)
		, m_brickHealth(_health)
	{
	}

	BrickBehaviour::~BrickBehaviour()
	{
	}

	void BrickBehaviour::Start()
	{
		m_renderer = new TextureRenderer();
		GetGameObject()->AddComponent(m_renderer);

		Texture* texture = GameApplication::GetInstance()->GetAssetManager().GetAssetAs<Texture>(AssetIDs::Brick);

		m_renderer->SetTexture(texture);

		m_bodyComponent = new PhysicsBodyComponent(Vec2(texture->GetWidth(), texture->GetHeight()));
		m_bodyComponent->SetBodyType(PhysicsBodyComponent::eBodyType_static);
		m_bodyComponent->OnCollideWithSignal().Connect(this, &BrickBehaviour::OnCollideWith);
		GetGameObject()->AddComponent(m_bodyComponent);

		UpdateColour();
	}

	void BrickBehaviour::Update()
	{
	}

	void BrickBehaviour::End()
	{
	}

	BrickBehaviour::OnBrickSignal& BrickBehaviour::OnBrickDestroyedSignal()
	{
		return m_onBrickDestroyed;
	}

	void BrickBehaviour::OnCollideWith(PhysicsBodyComponent* _other)
	{
		const std::string& objectName = _other->GetGameObject()->m_name;

		if (objectName == GameApplication::BallTag)
		{
			//--m_brickHealth;
			m_brickHealth -= 1;

			if (m_brickHealth <= 0)
			{
				m_onBrickDestroyed.Emit(this);
			}
			else
			{
				Texture* texture = GameApplication::GetInstance()->GetAssetManager().GetAssetAs<Texture>(AssetIDs::ParticleSquare);

				ParticleObject* particles = new ParticleObject("particles", "brick_hit");
				particles->SetParticleTexture(texture);
				particles->SetParticleColour(m_renderer->GetColour());
				particles->SetParent(GetGameObject());
				particles->StartEmitting(true, 250);

				m_particles.push_back(particles);

				UpdateColour();
			}
		}
	}

	void BrickBehaviour::UpdateColour()
	{
		switch (m_brickHealth)
		{
		case 1: m_renderer->SetColour(Colour::Blue); break;
		case 2: m_renderer->SetColour(Colour::Red); break;
		case 3: m_renderer->SetColour(Colour::Green); break;
		default: m_renderer->SetColour(Colour::Yellow); break;
		}
	}
}