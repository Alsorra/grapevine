/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

#ifndef BREAKOUT_ASSETIDS
#define BREAKOUT_ASSETIDS

namespace Breakout
{
	namespace AssetIDs
	{
		enum eAssetIDs
		{
			None = 0,

			TTF_Autumn,

			Bat,
			Ball,
			Brick,
			ParticleSquare
		};
	}
}

#endif // BREAKOUT_ASSETIDS