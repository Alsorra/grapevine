/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

#include "GameAssetManager.h"

#include "AssetIds.h"

#include <map>

namespace Breakout
{
	GameAssetManager::GameAssetManager()
		: AssetManager("Assets\\Breakout")
		, m_assetsLoaded(false)
	{
	}

	GameAssetManager::~GameAssetManager()
	{
	}

	void GameAssetManager::SetupAssets()
	{
		SetupAsset("Autumn.ttf", (unsigned)AssetIDs::TTF_Autumn);

		SetupAsset("Bat.png", (unsigned)AssetIDs::Bat);
		SetupAsset("Ball.png", (unsigned)AssetIDs::Ball);
		SetupAsset("Brick.png", (unsigned)AssetIDs::Brick);
		SetupAsset("ParticleSquare.png", (unsigned)AssetIDs::ParticleSquare);
	}
}