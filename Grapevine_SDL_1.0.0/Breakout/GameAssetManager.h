/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

#ifndef BREAKOUT_GAME_ASSET_MANAGER
#define BREAKOUT_GAME_ASSET_MANAGER

#include "../Engine/Application/AssetManager.h"

using namespace Grapevine;

namespace Breakout
{
	class GameAssetManager : public AssetManager
	{
		friend class GameApplication;

	private:
		GameAssetManager();
		~GameAssetManager();

	private:
		void SetupAssets() override;

	private:
		bool m_assetsLoaded;
	};
}

#endif // BREAKOUT_GAME_ASSET_MANAGER