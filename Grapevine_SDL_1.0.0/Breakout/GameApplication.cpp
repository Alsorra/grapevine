/*
 *
 * Breakout
 *
 * Written and Owned by Alice Thorntonsmith
 *
 */

// this class
#include "GameApplication.h"

// source includes
#include "AssetIDs.h"
#include "GameAssetManager.h"
#include "Behaviours/BallBehaviour.h"
#include "Behaviours/BatBehaviour.h"
#include "Behaviours/BrickBehaviour.h"

// engine includes
#include "../Engine/Components/Behaviours/Behaviour.h"
#include "../Engine/Components/Physics/PhysicsBodyComponent.h"
#include "../Engine/Components/Renderers/TextRenderer.h"
#include "../Engine/Components/Renderers/TextureRenderer.h"
#include "../Engine/Graphics/Texture.h"
#include "../Engine/Input/Input.h"
#include "../Engine/Json/JsonHelper.h"
#include "../Engine/Objects/GameObjectFactory.h"
#include "../Engine/Objects/ParticleObject.h"
#include "../Engine/Objects/World.h"

namespace Breakout
{
	std::string GameApplication::BatTag = "Bat";
	std::string GameApplication::BallTag = "Ball";
	std::string GameApplication::BrickTag = "Brick_Tag";
	std::string GameApplication::LeftWallTag = "Left_Wall";
	std::string GameApplication::RightWallTag = "Right_Wall";
	std::string GameApplication::CeilingTag = "Ceiling";
	std::string GameApplication::FloorTag = "Floor";

	GameApplication::GameApplication()
		: m_bat(nullptr)
		, m_ball(nullptr)
	{
	}

	GameApplication::~GameApplication()
	{
	}

	bool GameApplication::LoadMedia()
	{
		//Loading success flag
		bool success = true;

		GameAssetManager* manager = new GameAssetManager();

		success = manager->LoadAssets();

		mAssetManager.reset(manager);

		//Nothing to load
		return success;
	}

	void GameApplication::Start()
	{
		const float SCREEN_WIDTH = 1024.0f;
		const float SCREEN_HEIGHT = 768.0f;
		const float WALL_WIDTH = 20.0f;

		GameObject* leftWall = GameObjectFactory::GenerateEmptyObject(LeftWallTag, Transform(Vec2(10.0f, 384.0f)));
		leftWall->AddComponent(new PhysicsBodyComponent(Vec2(WALL_WIDTH, SCREEN_HEIGHT), PhysicsBodyComponent::eBodyType_static));

		GameObject* rightWall = GameObjectFactory::GenerateEmptyObject(RightWallTag, Transform(Vec2(1014.0f, 384.0f)));
		rightWall->AddComponent(new PhysicsBodyComponent(Vec2(WALL_WIDTH, SCREEN_HEIGHT), PhysicsBodyComponent::eBodyType_static));

		GameObject* ceiling = GameObjectFactory::GenerateEmptyObject(CeilingTag, Transform(Vec2(512.0f, 10.0f)));
		ceiling->AddComponent(new PhysicsBodyComponent(Vec2(SCREEN_WIDTH, WALL_WIDTH), PhysicsBodyComponent::eBodyType_static));

		GameObject* floor = GameObjectFactory::GenerateEmptyObject(FloorTag, Transform(Vec2(512.0f, 758.0f)));
		floor->AddComponent(new PhysicsBodyComponent(Vec2(SCREEN_WIDTH, WALL_WIDTH), PhysicsBodyComponent::eBodyType_static));

		m_bat = GameObjectFactory::GenerateEmptyObject();
		m_bat->m_name = BatTag;
		m_bat->AddComponent(new BatBehaviour());

		GameObject* ball = GameObjectFactory::GenerateEmptyObject();
		ball->m_name = BallTag;
		ball->AddComponent(new BallBehaviour());

		TrueTypeFont* font = GameApplication::GetInstance()->GetAssetManager().GetAssetAs<TrueTypeFont>(AssetIDs::TTF_Autumn);
		GameObject* text = GameObjectFactory::GenerateTextObject(font, "Breakout", Transform(Vec2(300.0f, 100.0f)));
		text->GetComponent<TextRenderer>()->SetColour(Colour::White);

		LoadLevel("Assets\\Levels\\002.json");
	}

	void GameApplication::Update()
	{
		for (std::vector<BrickBehaviour*>::iterator iter = m_destroyedBricks.begin(); iter != m_destroyedBricks.end(); ++iter)
		{
			(*iter)->GetGameObject()->SetParent(nullptr);
			delete (*iter)->GetGameObject();
		}
		m_destroyedBricks.clear();
	}

	void GameApplication::End()
	{
	}

	void GameApplication::OnBrickDestroyed(BrickBehaviour* _behaviour)
	{
		m_destroyedBricks.push_back(_behaviour);
	}

	void GameApplication::LoadLevel(const std::string& _path)
	{
		Json::Value level;
		if (JsonHelper::TryLoadJsonFromFile(_path, level))
		{
			const Json::Value& bricks = level["Bricks"];

			float row = 0;

			Texture* texture = GameApplication::GetInstance()->GetAssetManager().GetAssetAs<Texture>(AssetIDs::Brick);

			Vec2 startPosition(30.0f + ((float)texture->GetWidth() / 2.0f), 30.0f + ((float)texture->GetHeight() / 2.0f));

			Json::ValueIterator bricksIter = bricks.begin();
			while (bricksIter != bricks.end())
			{
				const Json::Value& rowJson = (*bricksIter);

				float column = 0;

				Json::ValueConstIterator rowIter = rowJson.begin();
				while (rowIter != rowJson.end())
				{
					int brickValue = (*rowIter).isNumeric() ? (*rowIter).asInt() : 0;

					if (brickValue > 0)
					{
						Vec2 pos(startPosition.x + ((float)texture->GetWidth() * column), startPosition.y + ((float)texture->GetHeight() * row));

						GameObject* brick = GameObjectFactory::GenerateEmptyObject(BrickTag, Transform(pos));

						BrickBehaviour* behaviour = new BrickBehaviour(brickValue);
						brick->AddComponent(behaviour);
						behaviour->OnBrickDestroyedSignal().Connect(this, &GameApplication::OnBrickDestroyed);
					}

					++column;
					++rowIter;
				}

				++row;
				++bricksIter;
			}
		}
	}
}